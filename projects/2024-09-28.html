<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="image/png" href="favicon.png" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
        <title>Tyler's website</title>
<meta property="og:title" content="FreeBSD Boot Environments" />
      <meta property="og:description" content="" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="/projects/2024-09-28" />
      <meta property="og:image" content="https://foxide.xyz/" />

    </head>

    <body>
        <header>
            <h1>Tyler's Site</h1>
        </header>
        <nav>
            <a href="https://foxide.xyz/">home</a>&emsp;
            <a href="https://foxide.xyz/articles.html">articles</a>&emsp;
            <a href="https://foxide.xyz/projects.html ">projects</a>&emsp;
            <a href="https://foxide.xyz/consulting.html">consulting</a>&emsp;
            <a href="https://git.foxide.xyz">code</a>
        </nav>
        <article>
<h1 id="abstract">Abstract</h1>
<p>FreeBSD’s Boot Environments feature is a tool that takes some of the
fear and uncertainty out of things like upgrades by taking a snapshot of
the root file system; in the event that the root file system needs to be
rolled back to a known good version, simply changing an option in the
boot menu will allow for that without affecting user files. This feature
is a massive benefit of using FreeBSD with ZFS-on-root. I recently ran
into an issue with upgrading my FreeBSD laptop and had to rely on a
previous boot environment, which inspired me to do a small write-up on
the topic.</p>
<h1 id="getting-started-with-boot-environments">Getting Started with
Boot Environments</h1>
<p>Getting started with boot environments is very easy these days; all
that is needed is a FreeBSD install with ZFS-on-root. Similar systems
are available on Linux, however, I do not know of distributions that
have an out-of-the-box equivalent that works as well as boot
environments. All currently supported FreeBSD systems (13.x, 14.x, as
well as 15-CURRENT) will enable boot environments by default when using
ZFS for the root file system, no further configuration required. During
major and minor version upgrades a boot environment of the previous (the
boot environment that was used before upgrading) is created. This will
allow the system administrator to rollback the upgrade if something did
not work as intended, or if the upgrade left the system completely
broken.</p>
<h1 id="creating-and-managing-boot-environments">Creating and Managing
Boot Environments</h1>
<p>The automatic backup of a boot environment is great for upgrading a
system, however, there are more reasons that you might want to create a
backup of the current boot environment. The simplest reason is to make
sure that the system is bootable after something (probably an
“experiment”) really stupid happens. Working with boot environments is
fairly straightforward:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="co"># This will create a boot environment called &#39;test01&#39;</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="ex">bectl</span> create test01</span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a><span class="co"># Then the environment can be listed with:</span></span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a><span class="ex">bectl</span> list</span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a><span class="co"># And to activate the boot environment:</span></span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a><span class="ex">bectl</span> activate test01</span></code></pre></div>
<p>While this process technically works, it is not an ideal process for
several reasons. The first reason is the manageability of the boot
environments; theoretically, the name shouldn’t really matter, but it is
foreseeable to have half a dozen or more boot environments. No one is
going to remember what ‘test01’ is or when it was created. Secondly,
what if there is an issue with the boot environment, and you cannot get
it boot back? That isn’t a problem if is a local machine, but for remote
machines, you’d like it to fail into a known good state, right? Let’s
see if we can improve this process a bit.</p>
<p>The first thing is the naming convention, it is silly, but many
experienced IT staff will say that creating a following a naming
convention is a key point of good IT practices. Names should mean
something and be able to communicate important information quickly, so,
keeping that in mind for our boot environments a good naming convention
might be:</p>
<p><span
class="math inline"><em>D</em><em>A</em><em>T</em><em>E</em>−</span>{ACTION},
for example <code>20240123-UPGRADE</code>. The example name would
communicate that the boot environment was created on January 23, 2024
and was created before upgrading the system. Next, let’s see about
fixing the failing boot issue:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Create the boot environment</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="ex">bectl</span> create 20240927-test</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="co"># Set the boot environment to be the snapshot that was just taken (current environment)</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a><span class="ex">bectl</span> activate 20240927-test</span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a><span class="co"># Then set the boot environment to boot the &quot;upgraded&quot; system just once (that is set by the &#39;-t&#39; flag)</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a><span class="ex">bectl</span> activate <span class="at">-t</span> default</span></code></pre></div>
<p>Assuming the upgrade worked as intended, the boot environment can be
permanently set by running <code>bectl activate default</code> (note the
lack of a ‘-t’).</p>
<h1 id="actually-booting-into-the-environment">Actually Booting into the
Environment</h1>
<p>In the event that this boot environment should be re-used, simply
reboot the machine and in the FreeBSD bootloader hit the ‘e’ key on the
keyboard. This will open another menu in which an alternative boot
environment can be selected by simply pressing ‘a’. Pressing ‘a’ will
change the environment name from <code>zfs:zroot/ROOT/default</code>, to
<code>zfs:zroot/ROOT/${BOOT_ENV_NAME}</code>. From there, press
backspace to return to the main section of the bootloader, and press
‘Enter’ to continue booting the computer. Once the computer is booted,
the root file system should return to the state in was in before
performing the upgrade. It is worth noting that <code>zroot/HOME</code>
is not affected by changing boot environments, meaning that any files in
<code>/home</code> will remain unaffected.</p>
<h1 id="a-creative-use-for-boot-environments">A Creative Use for Boot
Environments</h1>
<p>Using boot environments as a method to safely restore a broken system
is great, however, there are more creative things that it can be used
for. The main one that comes to mind is a method for atomic in-place
upgrades that Alan Jude came up with. I first heard the idea on an
episode of <a href="https://2.5admins.com/">2.5 Admins</a>, and it
sounded complex, but genius for managing a lot of remote machines. When
writing about boot environments I wanted to take a look at the method to
actually make this happen, and realized it is slightly more difficult
than I originally thought. Because of that, implementing this idea is
going to take a bit more time than I have allotted to getting this post
out, however, I do want to make it happen in the future. When I do, I am
going to come back to this post and describe the method that I used to
make it happen. For now, I have some resources in the below section that
will give more information on not only this idea, but boot environments
in general.</p>
<h1 id="resources">Resources</h1>
<ul>
<li><a href="https://wiki.freebsd.org/BootEnvironments">FreeBSD Wiki
entry on Boot Environments</a></li>
<li><a
href="https://papers.freebsd.org/2018/eurobsdcon/allanjude-bootenv_at_scale.files/allanjude-bootenv_at_scale.pdf">Boot
Environments at Scale: Alan Jude</a></li>
<li><a href="https://www.youtube.com/watch?v=YcdFln0vO4U">ZFS Powered
Magic Upgrades Using boot environments for atomic in-place
upgrades</a></li>
</ul>
</article>
<footer>
    <p>
        Comments or suggestions?<br />
        Contact me: <a href="mailto:tyler.clark@foxide.xyz">tyler.clark@foxide.xyz</a>
    </p>
</footer>
