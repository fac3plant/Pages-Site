<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="image/png" href="favicon.png" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
        <title>Tyler's website</title>
<meta property="og:title" content="Learning ZFS Part 2 - Working with ZFS Snapshots" />
      <meta property="og:description" content="" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="/projects/2024-08-03" />
      <meta property="og:image" content="https://foxide.xyz/" />

    </head>

    <body>
        <header>
            <h1>Tyler's Site</h1>
        </header>
        <nav>
            <a href="https://foxide.xyz/">home</a>&emsp;
            <a href="https://foxide.xyz/articles.html">articles</a>&emsp;
            <a href="https://foxide.xyz/projects.html ">projects</a>&emsp;
            <a href="https://foxide.xyz/consulting.html">consulting</a>&emsp;
            <a href="https://git.foxide.xyz">code</a>
        </nav>
        <article>
<h1 id="abstract">Abstract</h1>
<p>The <a href="https://foxide.xyz/blog/2024-07-20.html">last post</a>
of this series worked through replacing failed disks with ZFS. This post
will be on working with ZFS snapshots and data recovery using ZFS; for
the purposes of this post, I am going to be using a ZFS on root FreeBSD
machine and only working with the one <code>zroot</code> pool that is
the default in FreeBSD when installing ZFS on root.</p>
<h1 id="taking-snapshots-with-zfs">Taking snapshots with ZFS</h1>
<p>By default non-root users have very little privilege when it comes to
ZFS; this is a good thing, but makes permissions more annoying and is
overall bad practice. So, the first thing to do is to grant another user
necessary permissions within ZFS.</p>
<pre class="shell"><code># The following command must be run as root
# substitue ${USER} with the username that will need ZFS permission
zfs allow -u ${USER} canmount,compression,create,destroy,hold,mount,mountpoint,receive,send,snapshot zroot/ROOT</code></pre>
<p>For more information on what these permissions are/do, read the <a
href="https://openzfs.github.io/openzfs-docs/man/master/8/zfs-allow.8.html">zfs-allow
man page</a></p>
<p>Now the user specified in the command above should be able to create
a snapshot, as well as use some other features of ZFS. Taking a snapshot
of the <code>zroot/ROOT</code> pool is as simple as running:</p>
<pre class="shell"><code># This would take a snapshot of zroot/ROOT with the tag of bk01
zfs snapshot zroot/ROOT@bk01
# This would recursively take a snapshot of zroot/ROOT with the tag of bk02
zfs snapshot -r zroot/ROOT@bk02</code></pre>
<p>While that is great, that doesn’t actually back up the entire system.
Running a <code>zfs list</code> command on a standard zfs on root
install of FreeBSD will turn up something like the following:</p>
<pre class="shell"><code>zfs list
NAME                 USED  AVAIL  REFER  MOUNTPOINT
zroot                959M  25.7G    66K  /zroot
zroot/ROOT           956M  25.7G    66K  none
zroot/ROOT/default   956M  25.7G   951M  /
zroot/home           400K  25.7G    67K  /home
zroot/home/${USER}   333K  25.7G   179K  /home/${USER}
zroot/tmp            134K  25.7G    73K  /tmp
zroot/usr            198K  25.7G    66K  /usr
zroot/usr/ports       66K  25.7G    66K  /usr/ports
zroot/usr/src         66K  25.7G    66K  /usr/src
zroot/var            741K  25.7G    66K  /var
zroot/var/audit       68K  25.7G    68K  /var/audit
zroot/var/crash     66.5K  25.7G  66.5K  /var/crash
zroot/var/log        340K  25.7G   212K  /var/log
zroot/var/mail       134K  25.7G    77K  /var/mail
zroot/var/tmp         67K  25.7G    67K  /var/tmp</code></pre>
<p>The <code>zpool/ROOT</code> pool only contains the basic installation
files of the system; backing up from <code>zpool/ROOT@bk02</code> will
be missing the <code>/home</code>, <code>/tmp</code>, <code>/usr</code>,
and <code>/var</code> directories from the original system. Getting a
snapshot of those can be done by running the following:</p>
<pre class="shell"><code># This will create a full system snapshot with the tag full-bk01
zfs snapshot -r zroot@full-bk01</code></pre>
<p>To get a list of available snapshots on the system, run
<code>zfs list -t snapshot</code>. This is some test output from the VM
I am using for testing that had a snapshot taken of the entire system
called <code>bk01</code>:</p>
<pre class="shell"><code>zfs list -t snapshot
NAME                      USED  AVAIL  REFER  MOUNTPOINT
zroot@bk01                  0B      -    66K  -
zroot/ROOT@bk01             0B      -    66K  -
zroot/ROOT/default@bk01  4.99M      -   948M  -
zroot/home@bk01             0B      -    67K  -
zroot/home/${USER}@bk01   154K      -   180K  -
zroot/tmp@bk01             61K      -    73K  -
zroot/usr@bk01              0B      -    66K  -
zroot/usr/ports@bk01        0B      -    66K  -
zroot/usr/src@bk01          0B      -    66K  -
zroot/var@bk01              0B      -    66K  -
zroot/var/audit@bk01        0B      -    68K  -
zroot/var/crash@bk01        0B      -  66.5K  -
zroot/var/log@bk01        128K      -   177K  -
zroot/var/mail@bk01      56.5K      -  72.5K  -
zroot/var/tmp@bk01          0B      -    67K  -</code></pre>
<p>Next, let’s see how to send those snapshots off to some backup
storage in case the system fails.</p>
<h1 id="sending-snapshots-to-a-backup-server">Sending snapshots to a
backup server</h1>
<p>Having a backup file of a system is great, but it needs to be
accessible from another location in case the system fails so the
original machine can be restored to a functioning state. Thankfully
doing that with ZFS is decently straight-forward. The tool to do this is
<code>zfs send</code>; these examples only go over sending the snapshots
as files, however, they can also be sent as entire datasets to another
system using ZFS. I had issues doing this with a root on ZFS snapshot,
but that might have been a skill issue that could have been overcome by
someone that is more knowledgeable. To send the snapshot to a file, run
the following:</p>
<pre class="shell"><code># This will create a file called `full-backup.zfs` that will contain the snapshot `zpool@full-bk01`
# this snapshot can be sent with standard tools like scp, rsync, or stored on a fileshare.
zfs send -R zpool@full-bk01 &gt; full-backup.zfs
# This example will send the snapshot `zpool@full-bk01` to be compressed via gzip, then to a remote machine called `backup.server`
# The compressed file will then be available on that server
zfs send -R zpool@full-bk01 | gzip | ssh ${USER}@backup.server &quot;cat &gt; /backup/directory/location/full-bk01.zfs.gz&quot;</code></pre>
<p>Now that the snapshot has been moved to another system as a proper
backup; let’s go over how to actually use the snapshot to recover
lost/destroyed data.</p>
<h1 id="restoring-from-snapshots">Restoring from snapshots</h1>
<p>Now that we have a proper backup, and it is located elsewhere besides
the machine, we need to figure out how to actually utilize it like a
backup. There are three main cases that I am going to cover in this
particular blog post that should cover most situations.</p>
<h2 id="restoring-files-a-la-carte">Restoring files, a la carte</h2>
<p>This case assumes that some (probably user) error happened and the
system itself is okay, but some files were deleted or modified in a
negative way and need to be restored/recovered. Assuming we have a
snapshot that contains the necessary files (having regular snapshots
with a good naming convention is going to save you here), this can be
done very easily. ZFS has a hidden directory <code>.zfs</code>
throughout the file system for all the directories that a snapshot
exists for. Within that <code>.zfs</code> directory it will show the
different snapshots by their tag name (for example if the snapshots are:
<code>zroot@bk01</code> and <code>zroot@bk02</code> there will be
<code>bk01</code> and <code>bk02</code> directories), and within those
directories will be the files that were there at the time of the
snapshot. Accessing and restoring the files on the machine can be done
by:</p>
<pre class="shell"><code># assuming we are trying to recover `file.md` that was contained in snapshot `zroot@full-bk01`
# cd into the snapshot dir
cd ~/.zfs/snapshot/full-bk01
# copy the file back to the file regular file system
cp -r file.md ~/</code></pre>
<p>The simplicity of this file recovery method is amazing, it is odd
that the <code>.zfs</code> directory doesn’t show up when running a
<code>ls -a</code>, but running a <code>cd .zfs</code> will work.
However, recovering files feels like just a normal and mundane file
operation that any Linux or BSD user should be comfortable with.</p>
<h2 id="rolling-back-the-file-system">Rolling back the file system</h2>
<p>This example assumes that something has gone quite wrong, and the
entire system needs to be taken back to a previous snapshot state (this
could also be done with specific datasets,
i.e. <code>zpool/home</code>). For this we use the
<code>zfs rollback</code> command. It is also worth noting that I did
not set those permissions up for a non-root user in this blog post, so
unless you see <code>rollback</code> somewhere when running
<code>zfs allow</code> on your pool and/or dataset, then you will have
to run it as root as well. I think I prefer rollbacks being segregated
to root users as it could potentially be damaging, and people should
think about commands more carefully when running as root anyway, but to
roll the system back run:</p>
<pre class="shell"><code>zfs rollback -r zroot@full-bk01</code></pre>
<p>This will bring the entire system to the state that it was in during
the time of taking the snapshot <code>zroot@full-bk01</code>. To
rollback a specific dataset:</p>
<pre class="shell"><code># assuming rolling back the zroot/tmp dataset at snapshot full-bk01
zfs rollback -r zroot/tmp@full-bk01</code></pre>
<h2 id="entire-root-file-system">Entire root file system</h2>
<p>The final example assumes that the entire system has been
corrupted/deleted/caught fire/nuked/etc and has to be fully recovered
from backup onto a brand new hard drive. Make sure the new drive is
connected to the system, and boot a live FreeBSD environment. I used <a
href="https://mfsbsd.vx.sk/">mfsBSD</a> (found thanks to <a
href="https://hashbang0.com/2019/02/07/restore-freebsd-from-a-zfs-snapshot/">this</a>
blog post) as it allowed me to mount a zpool to <code>/mnt</code> where
as the standard live FreeBSD system would not allow that as it was
read-only.</p>
<pre class="shell"><code># assuming the drive is `ada0`
# also assuming a basic FreeBSD installation partition scheme
gpart create -s gpt ada0
gpart add -t freebsd-boot -s 512k ada0
gpart add -t freebsd-swap -s 2G ada0
gpart add -t freebsd-zfs -l disk0 ada0
# don&#39;t forget this otherwise system will not boot
gpart bootcode -b /boot/pmbr -p /boot/gptzfsboot -i 1 ada0</code></pre>
<p>From here, we are ready to actually get the backup onto the system
and restore it. It’s worth noting that there are a lot of different ways
to get the snapshot file from the backup machine onto the machine we are
trying to recover (recovery machine). We can do a pull, where we are
logged into the recovery machine and transfer the file from the backup
server to the recovery machine by “pulling” it off. We can do a push,
where we log into the backup machine, and push a copy of the file onto
the recovery machine. We could also do something like transfer the file
via USB flash drive.</p>
<pre class="shell"><code>zpool create -d -o altroot=/mnt zroot ada0p3
# Example of a &quot;pull&quot;, pulling snapshot off of the backup server using the recovery machine
ssh ${USER}@backup.server &quot;cat /path/to/backup/full-bk01.zfs.gz | gunzip | zfs receive -vF zroot&quot;
# Example of a push, pushing the snapshot from the backup server onto the recovery machine
cat /path/to/backup/full-bk01.zfs.gz | gunzip | ssh ${USER}@recovery.machine &quot;zfs receive -vF zroot&quot;
# also do not forget this otherwise system will not boot
zpool set bootfs=zroot/ROOT/default zroot</code></pre>
<h1 id="closing-thoughts">Closing thoughts</h1>
<p>ZFS is a lot easier to learn than I thought it would be, however,
that does not make it simple. It is definitely a great choice for a sys
admin trying to make life easier once the sys admin knows how to use
it.</p>
<h1 id="researching-resources">Researching resources</h1>
<p>Some of the resources that I found helpful while working on this blog
post.</p>
<ul>
<li>zfs and zpool man pages on FreeBSD system</li>
<li><a
href="https://openzfs.github.io/openzfs-docs/man/master/index.html">OpenZFS
man pages</a> - Good for general reference regarding zfs commands</li>
<li><a
href="https://hashbang0.com/2019/02/07/restore-freebsd-from-a-zfs-snapshot/">Restoring
FreeBSD from a ZFS snapshot</a> - This blog explained recovering an
entire ZFS system from a snapshot.</li>
<li><a
href="https://dan.langille.org/2015/02/16/zfs-send-zfs-receive-as-non-root/">zfs
send | zfs receive as non-root</a> - Blog that helped explain some zfs
permissions, and showed different send/receive examples. ## Other
mentions</li>
<li><a href="https://mfsbsd.vx.sk/">mfsBSD</a> - Small BSD distro that
is useful in helping recover from ZFS snapshots; allows mounting a zpool
to <code>/mnt</code> while the live FreeBSD installer ISO will not allow
it.</li>
</ul>
</article>
<footer>
    <p>
        Comments or suggestions?<br />
        Contact me: <a href="mailto:tyler.clark@foxide.xyz">tyler.clark@foxide.xyz</a>
    </p>
</footer>
