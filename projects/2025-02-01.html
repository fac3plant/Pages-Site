<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="image/png" href="favicon.png" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
        <title>Tyler's website</title>
<meta property="og:title" content="Disk Encryption with Cryptsetup" />
      <meta property="og:description" content="" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="/projects/2025-02-01" />
      <meta property="og:image" content="https://foxide.xyz/" />

    </head>

    <body>
        <header>
            <h1>Tyler's Site</h1>
        </header>
        <nav>
            <a href="https://foxide.xyz/">home</a>&emsp;
            <a href="https://foxide.xyz/articles.html">articles</a>&emsp;
            <a href="https://foxide.xyz/projects.html ">projects</a>&emsp;
            <a href="https://foxide.xyz/consulting.html">consulting</a>&emsp;
            <a href="https://git.foxide.xyz">code</a>
        </nav>
        <article>
<h1 id="abstract">Abstract</h1>
<p>Applying good disk encryption is a great way to protect data at rest.
This is important in a variety of contexts; whether it be a journalist
protecting a source’s story, a company protecting company assets, or
just a person protecting their right to privacy. Good disk encryption
can provide a layer of protection in all of these situations, but how
does one go about setting up disk encryption on Linux? The standardized
method of doing this is by using <a
href="https://www.kernel.org/pub/linux/utils/cryptsetup/LUKS_docs/on-disk-format.pdf">LUKS</a>
and <a
href="https://gitlab.com/cryptsetup/cryptsetup/-/blob/main/FAQ.md">cryptsetup</a></p>
<h1 id="what-is-luks-and-cryptsetup">What is LUKS and Cryptsetup?</h1>
<h2 id="some-notes-before-getting-started">Some Notes Before Getting
Started</h2>
<p>Cryptography is a very difficult field that requires a lot of study
and mathematics. Cryptsetup allows users to handle their own encryption
via the <code>dm-crypt</code> kernel module, however, this is
specifically pointed out in the <code>cryptsetup</code> man page, as
well as in the FAQ:</p>
<blockquote>
<p>Unless you understand the cryptographic background well, use LUKS.
With plain dm-crypt there are a number of possible user errors that
massively decrease security. While LUKS cannot fix them all, it can
lessen the impact for many of them.</p>
</blockquote>
<p>Effectively, if you aren’t sure that you understand cryptography,
don’t use plain dm-crypt and expect it to be secure. You will most
likely do it wrong and leave your disks vulnerable to avoidable
attacks.</p>
<p>This blog post will only cover using LUKS as I do not have enough
background in cryptography to even begin to explain how to properly use
plain dm-crypt.</p>
<h1 id="examples">Examples</h1>
<p>The commands for encrypting a disk (or really any block device) are
not particularly difficult.</p>
<h2 id="encrypting-a-disk">Encrypting a disk</h2>
<p>The following commands show how to encrypt a disk located at
<code>/dev/sda</code>, then create a file system and mount it at
<code>/mnt</code>:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Run as root</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a><span class="co"># Format the entire drive with LUKS1 encryption</span></span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a><span class="ex">cryptsetup</span> luksFormat <span class="at">--type</span> luks1 /dev/sda</span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a><span class="co"># Open the encrypted block device</span></span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a><span class="ex">cryptsetup</span> luksOpen /dev/sda drive</span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a><span class="co"># Create a file system on the now decrypted device</span></span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a><span class="ex">mkfs.ext4</span> /dev/mapper/drive</span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true" tabindex="-1"></a><span class="co"># Mount the device</span></span>
<span id="cb1-10"><a href="#cb1-10" aria-hidden="true" tabindex="-1"></a><span class="fu">mount</span> /dev/mapper/drive /mnt</span></code></pre></div>
<p>It is worth noting, the <code>drive</code> that follows the
<code>/dev/sda</code> is just a label and can be anything as long as
there are not duplicate files in <code>/dev/mapper</code> (where
cryptsetup maps decrypted devices). <code>drive</code> will be used for
the remainder of this post, but know that it could be any other
label.</p>
<h2 id="using-a-key-file">Using a Key File</h2>
<p>Additionally, a keyfile can be created to unlock the encrypted volume
without having to type a password. <strong>NOTE</strong>, this command
uses <code>dd</code>, which can and will happily overwrite anything that
you point it at with no warnings. Before running the command, please
make sure the syntax is correct, specifically the <code>of</code>
location, as that is the location where the data will be written. Pro
tip, <code>if</code> stands for ‘input file’ and <code>of</code> stands
for ‘output file’.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Run as root</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="co"># Create the keyfile with pseudo random data</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a><span class="fu">dd</span> bs=1 count=64 if=/dev/urandom of=<span class="va">${HOME}</span>/keyfile</span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a><span class="co"># Add the key to the current encrypted volume</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a><span class="ex">cryptsetup</span> luksAddKey /dev/sda <span class="va">${HOME}</span>/keyfile</span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a><span class="co"># Then use this command to unlock the encrypted volume with the key</span></span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true" tabindex="-1"></a><span class="ex">cryptsetup</span> luksOpen /dev/sda drive <span class="at">--key-file</span> <span class="va">${HOME}</span>/keyfile</span></code></pre></div>
<p>Additionally, another passphrase can be added rather than a key by
just removing the location of the keyfile:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Run as root</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="ex">cryptsetup</span> luksAddKey /dev/sda</span></code></pre></div>
<h2 id="changing-the-passphrase">Changing the passphrase</h2>
<p>At some point, changing or removing a key (either passphrase or a
keyfile) may be required for one reason or another. This can be done by
running the following command:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Run as root</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a><span class="co"># This command is for changing a passphrase</span></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a><span class="ex">cryptsetup</span> luksChangeKey /dev/sda</span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a><span class="co"># This command will replace an existing passphrase with a keyfile</span></span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a><span class="ex">cryptsetup</span> luksChangeKey /dev/sda <span class="va">${HOME}</span>/keyfile</span></code></pre></div>
<p>Cryptsetup will then ask the user to enter the passphrase that needs
to be changed, then a new passphrase to replace it with. However,
changing a keyfile, or replacing a keyfile with a passphrase is slightly
less straightforward. The process would actually look more like removing
the old keyfile and adding a new keyfile or passphrase to the encrypted
volume:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Run as root</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a><span class="co"># Removing old keyfile</span></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a><span class="ex">cryptsetup</span> luksRemoveKey /dev/sda <span class="va">${HOME}</span>/keyfile</span></code></pre></div>
<p>Then add the new keyfile or passphrase as shown previously.</p>
<h1 id="backups">Backups</h1>
<p>As most everyone should know in 2025, backups are important, and this
is true even with encrypted data; but how does one backup encrypted
data? That’s a good question, that has two basic answers.</p>
<ol type="1">
<li>Backup the entire encrypted disk: Basically, just <code>dd</code>
the entire disk to another storage pool as the backup, if restoration is
needed, just <code>dd</code> it back onto a disk and carry on. This has
the benefit of the files staying encrypted, however, it is going to take
a lot more space and doing differential backups is not really possible
(at least to my knowledge).</li>
<li>Backup just the files on the encrypted disk. This has the benefit of
keeping storage space for the backups to a minimum, but the encryption
will have to be handled separately from the original files.</li>
</ol>
<p>Once the preferred type of backup is chosen, the specifics are really
just which tools are going to be used. There are many different backup
tools for Linux and other systems that use cryptsetup, so I am not going
to go in-depth on that.</p>
<h1 id="guides-for-encrypted-root">Guides for Encrypted Root</h1>
<p>An Linux install with full disk encryption is not very difficult to
do. Quite a few distros with proper installers have the option for it,
but some distros don’t. I am leaving two guides that should be useful in
getting started. The basic idea is just to use <code>cryptsetup</code>
to encrypt the disk, create your partitions and file systems, then
install onto those file systems. Both the Arch wiki and the Void Linux
docs page have guides on how to do this that are probably better than
what I would produce.</p>
<ul>
<li><a
href="https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LUKS_on_a_partition">Arch
Wiki page on full disk encryption</a></li>
<li><a
href="https://docs.voidlinux.org/installation/guides/fde.html">Void
Linux page on full disk encryption</a></li>
</ul>
<h1 id="simple-right">Simple, right?</h1>
<p>Sure, the commands are simple enough to follow, unfortunately the
tricky bit with cryptography is understanding the reasons behind what
you are doing. Why use LUKS1 over LUKS2? Which algorithms are secure?
For those answers, I lean on people smarter than myself on the topic…
the team that made <code>cryptsetup</code>. They have an <a
href="https://gitlab.com/cryptsetup/cryptsetup/-/blob/main/FAQ.md">FAQ</a>
page that answers most any question that you could dream up for the
tool. Additionally, if you dream up a question that is not on that list,
there is a <a
href="https://subspace.kernel.org/lists.linux.dev.html">mailing list</a>
that questions are welcomed at.</p>
<h1 id="resources">Resources</h1>
<ul>
<li><a
href="https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Cryptsetup_usage">Arch
wiki page for cryptsetup</a></li>
<li><a
href="https://gitlab.com/cryptsetup/cryptsetup/-/wikis/FrequentlyAskedQuestions">Cryptsetup
FAQ</a></li>
<li><a
href="https://www.kernel.org/pub/linux/utils/cryptsetup/LUKS_docs/on-disk-format.pdf">LUKS1
White paper</a></li>
<li><a
href="https://gitlab.com/cryptsetup/LUKS2-docs/-/blob/main/luks2_doc_wip.pdf?ref_type=heads">LUKS2
White paper Draft</a></li>
</ul>
</article>
<footer>
    <p>
        Comments or suggestions?<br />
        Contact me: <a href="mailto:tyler.clark@foxide.xyz">tyler.clark@foxide.xyz</a>
    </p>
</footer>
