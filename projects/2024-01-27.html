<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="image/png" href="favicon.png" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
        <title>Tyler's website</title>
<meta property="og:title" content="Self-hosting Git Servers Part 1" />
      <meta property="og:description" content="" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="/projects/2024-01-27" />
      <meta property="og:image" content="https://foxide.xyz/" />

    </head>

    <body>
        <header>
            <h1>Tyler's Site</h1>
        </header>
        <nav>
            <a href="https://foxide.xyz/">home</a>&emsp;
            <a href="https://foxide.xyz/articles.html">articles</a>&emsp;
            <a href="https://foxide.xyz/projects.html ">projects</a>&emsp;
            <a href="https://foxide.xyz/consulting.html">consulting</a>&emsp;
            <a href="https://git.foxide.xyz">code</a>
        </nav>
        <article>
<h1 id="abstract">Abstract</h1>
<p>Git is an extremely valuable tool for devs and admins to be able to
manage changes to their configs and code. There are many options for Git
hosting, however, many options that people hear about are proprietary
with little to no self-hosting options. While there isn’t anything wrong
with that per se, self-hosting is a better option in my opinion. So, in
this post we are going to look at using <a
href="https://forgejo.org/">Forgejo</a> (a fork of Gitea made by the
folks at <a href="https://codeberg.org/">codeberg.org</a>) to self-host
Git services with many of the same features offered by something like
GitHub or GitLab. <a href="https://about.gitea.com/">Gitea</a> is
another product that would work, however, there have been some <a
href="https://gitea-open-letter.coding.social/">disagreements</a>
between some of the former Gitea maintainers and the company behind the
Gitea project, Gitea Ltd. Additionally, setting up Gitea is well
documented on their website, <a
href="https://docs.gitea.com/category/installation">here</a>.</p>
<p>The host for these notes is FreeBSD 14.0-RELEASE-p3 and has an IP
address of 192.168.122.11.</p>
<h1 id="hosting-with-forgejo">Hosting with Forgejo</h1>
<p>As of the time of writing, Forgejo is not available via pkg in
FreeBSD, but it did not seem particularly difficult to compile myself.
Really the only things that are needed to be installed are: Git, Go
(version 1.21 or newer), and npm 20. So, here is what the process for
installing the dependencies and compiling Forgejo look like:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Always a good idea to make sure packages are up-to-date</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="fu">sudo</span> pkg update</span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a><span class="fu">sudo</span> pkg upgrade</span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a><span class="co"># You must use go121, as the standard go package is not a new enough version</span></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a><span class="fu">sudo</span> pkg install npm-node20 go121 git</span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a><span class="co"># Symlinking go121 to go is required to pass the go check and make</span></span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a><span class="co"># Forgejo recognize that go is installed</span></span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a><span class="fu">sudo</span> ln <span class="at">-s</span> /usr/local/bin/go121 /usr/local/bin/go</span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true" tabindex="-1"></a><span class="co"># Clone the Forgejo repo</span></span>
<span id="cb1-10"><a href="#cb1-10" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> clone https://codeberg.org/forgejo/forgejo <span class="kw">&amp;&amp;</span> <span class="bu">cd</span> forgejo</span>
<span id="cb1-11"><a href="#cb1-11" aria-hidden="true" tabindex="-1"></a><span class="va">TAGS</span><span class="op">=</span><span class="st">&quot;bindata sqlite sqlite_unlock_notify&quot;</span> <span class="fu">make</span> build</span>
<span id="cb1-12"><a href="#cb1-12" aria-hidden="true" tabindex="-1"></a><span class="ex">./gitea</span></span></code></pre></div>
<p>From here, open a web browser to complete the setup. Navigate to the
IP address of the server with the port 3000 (e.g. 192.168.122.11:3000).
We can use the <code>daemon</code> utility to run the gitea binary in
the background using the following command:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">daemon</span> <span class="at">-f</span> ./gitea</span></code></pre></div>
<p>Alternatively, we can make a FreeBSD service to be able to manage the
application better. This is a very basic service file that is based on
the service file from installing <code>gitea</code> on a FreeBSD
machine. To create the service file simply run
<code>touch /usr/local/etc/rc.d/gitea &amp;&amp; chmod +x /usr/local/etc/rc.d/gitea</code>
as root, then add the following into the file:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="co">#!/bin/sh</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="co"># PROVIDE: gitea</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a><span class="co"># REQUIRE: NETWORKING SYSLOG</span></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a><span class="co"># KEYWORD: shutdown</span></span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a><span class="co">#</span></span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a><span class="co"># Add the following lines to /etc/rc.conf to enable gitea:</span></span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a><span class="co">#</span></span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a><span class="co">#gitea_enable=&quot;YES&quot;</span></span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true" tabindex="-1"></a><span class="bu">.</span> /etc/rc.subr</span>
<span id="cb3-11"><a href="#cb3-11" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-12"><a href="#cb3-12" aria-hidden="true" tabindex="-1"></a><span class="va">name</span><span class="op">=</span><span class="st">&quot;gitea&quot;</span></span>
<span id="cb3-13"><a href="#cb3-13" aria-hidden="true" tabindex="-1"></a><span class="va">rcvar</span><span class="op">=</span><span class="st">&quot;gitea_enable&quot;</span></span>
<span id="cb3-14"><a href="#cb3-14" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-15"><a href="#cb3-15" aria-hidden="true" tabindex="-1"></a><span class="ex">load_rc_config</span> <span class="va">$name</span></span>
<span id="cb3-16"><a href="#cb3-16" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-17"><a href="#cb3-17" aria-hidden="true" tabindex="-1"></a><span class="bu">:</span> <span class="va">${gitea_user</span><span class="op">:=</span><span class="st">&quot;git&quot;</span><span class="va">}</span></span>
<span id="cb3-18"><a href="#cb3-18" aria-hidden="true" tabindex="-1"></a><span class="bu">:</span> <span class="va">${gitea_enable</span><span class="op">:=</span><span class="st">&quot;NO&quot;</span><span class="va">}</span></span>
<span id="cb3-19"><a href="#cb3-19" aria-hidden="true" tabindex="-1"></a><span class="bu">:</span> <span class="va">${gitea_facility</span><span class="op">:=</span><span class="st">&quot;daemon&quot;</span><span class="va">}</span></span>
<span id="cb3-20"><a href="#cb3-20" aria-hidden="true" tabindex="-1"></a><span class="bu">:</span> <span class="va">${gitea_priority</span><span class="op">:=</span><span class="st">&quot;debug&quot;</span><span class="va">}</span></span>
<span id="cb3-21"><a href="#cb3-21" aria-hidden="true" tabindex="-1"></a><span class="bu">:</span> <span class="va">${gitea_shared</span><span class="op">:=</span><span class="st">&quot;/usr/local/share/</span><span class="va">${name}</span><span class="st">&quot;</span><span class="va">}</span></span>
<span id="cb3-22"><a href="#cb3-22" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-23"><a href="#cb3-23" aria-hidden="true" tabindex="-1"></a><span class="va">command</span><span class="op">=</span><span class="st">&quot;/usr/local/bin/gitea&quot;</span></span>
<span id="cb3-24"><a href="#cb3-24" aria-hidden="true" tabindex="-1"></a><span class="va">pidfile</span><span class="op">=</span><span class="st">&quot;/var/run/</span><span class="va">${name}</span><span class="st">.pid&quot;</span></span>
<span id="cb3-25"><a href="#cb3-25" aria-hidden="true" tabindex="-1"></a><span class="va">start_cmd</span><span class="op">=</span><span class="st">&quot;</span><span class="va">${name}</span><span class="st">_start&quot;</span></span>
<span id="cb3-26"><a href="#cb3-26" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-27"><a href="#cb3-27" aria-hidden="true" tabindex="-1"></a><span class="fu">gitea_start()</span> <span class="kw">{</span></span>
<span id="cb3-28"><a href="#cb3-28" aria-hidden="true" tabindex="-1"></a>  <span class="cf">for</span> d <span class="kw">in</span> /var/db/gitea /var/log/gitea<span class="kw">;</span> <span class="cf">do</span></span>
<span id="cb3-29"><a href="#cb3-29" aria-hidden="true" tabindex="-1"></a>    <span class="cf">if</span> <span class="bu">[</span> <span class="ot">!</span> <span class="ot">-e</span> <span class="st">&quot;</span><span class="va">$d</span><span class="st">&quot;</span> <span class="bu">]</span><span class="kw">;</span> <span class="cf">then</span></span>
<span id="cb3-30"><a href="#cb3-30" aria-hidden="true" tabindex="-1"></a>      <span class="fu">mkdir</span> <span class="st">&quot;</span><span class="va">$d</span><span class="st">&quot;</span></span>
<span id="cb3-31"><a href="#cb3-31" aria-hidden="true" tabindex="-1"></a>      <span class="fu">chown</span> <span class="va">${gitea_user}</span> <span class="st">&quot;</span><span class="va">$d</span><span class="st">&quot;</span></span>
<span id="cb3-32"><a href="#cb3-32" aria-hidden="true" tabindex="-1"></a>    <span class="cf">fi</span></span>
<span id="cb3-33"><a href="#cb3-33" aria-hidden="true" tabindex="-1"></a>  <span class="cf">done</span></span>
<span id="cb3-34"><a href="#cb3-34" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-35"><a href="#cb3-35" aria-hidden="true" tabindex="-1"></a>  <span class="ex">daemon</span> <span class="at">-T</span> <span class="va">${name}</span> <span class="dt">\</span></span>
<span id="cb3-36"><a href="#cb3-36" aria-hidden="true" tabindex="-1"></a>  <span class="at">-u</span> <span class="va">${gitea_user}</span> <span class="at">-p</span> <span class="va">${pidfile}</span> <span class="dt">\</span></span>
<span id="cb3-37"><a href="#cb3-37" aria-hidden="true" tabindex="-1"></a>  <span class="va">$command</span> <span class="at">--work-path</span> <span class="va">${gitea_shared}</span></span>
<span id="cb3-38"><a href="#cb3-38" aria-hidden="true" tabindex="-1"></a><span class="kw">}</span></span>
<span id="cb3-39"><a href="#cb3-39" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-40"><a href="#cb3-40" aria-hidden="true" tabindex="-1"></a><span class="ex">run_rc_command</span> <span class="st">&quot;</span><span class="va">$1</span><span class="st">&quot;</span></span></code></pre></div>
<p>From there, add <code>gitea_enable="YES"</code> into
<code>/etc/rc.conf</code>, then start the service with
<code>service gitea start</code>. The setup will have to be re-done
unless the service was pointed to the directory of the initial setup or
the files were moved to the new directory. Moving forward with this
setup, it will just be important to remember to update the software from
time-to-time by running <code>git pull origin master</code> and
recompiling from time-to-time (probably at least monthly). If and when
there is a ports version or a binary, it would be easier to switch to
that so that the software updates with the rest of the OS, but until
then, this solution will work.</p>
<p>In the next article I will be going over making a more minimalist Git
server using Git daemon.</p>
</article>
<footer>
    <p>
        Comments or suggestions?<br />
        Contact me: <a href="mailto:tyler.clark@foxide.xyz">tyler.clark@foxide.xyz</a>
    </p>
</footer>
