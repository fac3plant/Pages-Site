<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="image/png" href="favicon.png" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
        <title>Tyler's website</title>
<meta property="og:title" content="Basic Concepts of Running Email Service" />
      <meta property="og:description" content="" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="/projects/2024-12-06" />
      <meta property="og:image" content="https://foxide.xyz/" />

    </head>

    <body>
        <header>
            <h1>Tyler's Site</h1>
        </header>
        <nav>
            <a href="https://foxide.xyz/">home</a>&emsp;
            <a href="https://foxide.xyz/articles.html">articles</a>&emsp;
            <a href="https://foxide.xyz/projects.html ">projects</a>&emsp;
            <a href="https://foxide.xyz/consulting.html">consulting</a>&emsp;
            <a href="https://git.foxide.xyz">code</a>
        </nav>
        <article>
<h1 id="abstract">Abstract</h1>
<p>Learning to run and maintain an email system can be a daunting task,
especially for a young IT professional that potentially did not use
email before starting their career (me). That are a lot of prerequisite
skills that are required to get the service to run at all, then there
are more pieces of knowledge that must be sprinkled in to make the
service actually useful (by getting mail delivered and blocking spam). I
did a write-up on getting email setup on a previous <a
href="https://foxide.xyz/projects/2024-07-05.html">blog post</a>,
however, that post did not really explain any of the logic that went
into setting up an email server. This article intends to provide that
basic overview of how the various pieces of email fit together to make a
working system. It is also worth noting that this post will not go
in-depth on configurations for anything as this is supposed to be a
general overview; many of the configuration items that are required can
be found online for the platform you are using. Additionally, I will
have more resources that go more in-depth on email at the end of this
post.</p>
<h1 id="prerequisite-skills">Prerequisite Skills</h1>
<p>As eluded to in the intro paragraph, email is built on various other
skills that are critical to being able to run a successful mail service.
These skills are:</p>
<ul>
<li>System Administration for relevant operating system(s)</li>
<li>Domain Name System (DNS)</li>
<li>Networking</li>
<li>Understanding SSL/TLS (and which one you should be using) is a
plus</li>
</ul>
<h1 id="basic-requirements-for-email-server">Basic Requirements for
Email Server</h1>
<p>In earlier days of the Internet, the list of requirements for running
an email server was pretty short. You basically just needed a server, a
domain name, and some software to send and receive email (SMTP and
POP3/IMAP server). Once that is setup, then you can be on your way
sending all the email that you want. Technically that is still the case;
however in practice, the requirements are a bit higher today for emails
to actually be received by major providers such as Gmail, Yahoo, and
Outlook/Hotmail.</p>
<h2 id="hard-requirements">Hard Requirements</h2>
<p>This list is a minimal list of requirements to have the technical
capability to send and receive email.</p>
<ul>
<li>Domain Name: The server that is receiving the email should have a
domain name that is available on the hosts that will be sending email to
the server. The domain is probably not a “hard” requirement if email is
only being send internally, but then you are forced to use IP addresses
as the domain, which is terrible to work with. This is even less
practical on the open Internet, as not only would people have to type
your public IP to send anything to you, you also couldn’t do any of the
soft requirements in the next section. Without those, email will likely
be blocked by a large group of people you may want to send messages to.
Specifically, you want an MX record for your domain.</li>
<li>IMAP Server: An Internet Message Access Protocol (IMAP) server. This
is what will actually be receiving the mail that is sent to your server;
without it, the email server will effectively be deaf, and not able to
actually receive any messages. There are quite a few IMAP servers out
there, but one of the most popular IMAP servers for Linux/*BSD is <a
href="https://github.com/dovecot/core">Dovecot</a>. In Microsoft world,
the IMAP and SMTP server will almost certainly be Exchange, which can be
hosted locally (though they are attempting to move everyone into the
cloud).</li>
<li>SMTP: Simple Mail Transfer Protocol (SMTP). This is what is
responsible for sending email. Traditionally Sendmail was the tool of
choice for Unix admins, however, much like IMAP, there are a variety of
options to choose from. And again, like IMAP, in the land of Microsoft
the only choice is Exchange.</li>
</ul>
<h2 id="soft-requirements">Soft Requirements</h2>
<p>While these items are not technically required to have mail sent to
another email server, for the mail to actually be accepted and received
by other email servers on the Internet, these requirements must be
met.</p>
<ul>
<li>TLS: Transport Layer Security (TLS), specifically TLS 1.2 or higher
as other versions are deprecated because of weak cryptography. Thanks to
<a href="https://letsencrypt.org/">Let’s Encrypt</a>, there is
absolutely no reason to not have TLS on your website, your email, and
anything else that might benefit from it.</li>
<li>SPF: Sender Policy Framework (SPF) is a DNS authentication protocol
that declares a list of valid senders for a particular email domain. SPF
can also give hints to remote servers what should be done if the email
domain in question does not pass SPF. Some explanation on creating an
SPF record with some examples can be found <a
href="https://mxtoolbox.com/dmarc/spf/what-is-an-spf-record">here</a>.</li>
<li>DKIM: DomainKeys Identified Mail (DKIM) is another DNS
authentication protocol that cryptographically authenticates the
domain’s emails via public/private key pairs. Some explanation on
creating DKIM records with examples can be found <a
href="https://mxtoolbox.com/dmarc/dkim/setup/how-to-setup-dkim">here</a>.</li>
<li>DMARC: Domain-based Message Authentication Reporting and Conformance
(DMARC) is an email authentication protocol that works in conjunction
with DKIM and SPF to help prevent email domain spoofing. It does this by
instructing remote mail servers what to do if the received email does
not pass DKIM or SPF, as well as what to do if there is a misalignment
of either. A bit more explanation on how DMARC works and how it can be
configured for your domain can be found <a
href="https://dmarc.org/overview/">here</a>.</li>
</ul>
<h2 id="checking-your-records">Checking your records</h2>
<p>Once you <em>think</em> your records are setup for your email domain,
it is then time to check it using online tools. This <a
href="https://mxtoolbox.com/emailhealth">Email health tool from MX
Toolbox</a> is a very solid one that will show a lot of information
about your email domain and point out issues that should be addressed.
Example output from my mail server:</p>
<figure>
<img src="imgs/2024-12-02_01.png"
alt="MX Toolbox Health Check for Foxide.xyz" />
<figcaption aria-hidden="true">MX Toolbox Health Check for
Foxide.xyz</figcaption>
</figure>
<p>It is highly recommend to make sure the checks on this health check
tool pass before changing SPF to hard-fail and DMARC to reject or
quarantine as improper configuration of DNS records can affect mail
deliverability. If something is not setup correctly on the DNS records,
MX Toolbox will very helpfully offer some additionally info to clear up
what the problem might be.</p>
<h1 id="handling-spam">Handling Spam</h1>
<p>Most people hate email spam; and the reasons for this are two fold.
The obvious reason is because you have to deal with the spam in your
email box. However, the second reason is that email spam has been a huge
driving force to make email the headache that it is today. The idea was
that by having email providers declare where email will come from and
showing some validation that it was really sent from their email server
(SPF and DKIM) it would cut down on spoofing and spam. The ironic thing,
is that spammers tend to have the best SPF, DKIM, and DMARC records, and
will generally allow anyone to send email on their behalf, and will
actively tell other email server to ignore DKIM.</p>
<h2 id="spam-filters">Spam filters</h2>
<p>Since SPF, DKIM, and DMARC do little to prevent spam, the option is
an email spam filter. While this isn’t required per se, in reality this
is something you will likely want sooner or later, especially if your
email domain is associated with a business. Spam filters come in
software form or a hardware appliance; each of the different options for
a spam filter will work slightly differently, however, in general spam
filters will analyze incoming email and give it a spam score. This score
will determine whether the email will be considered spam by the filter;
in addition to this, actions can be setup on a tier system, so an email
with a lower score could be handled differently than an email with a
higher score.</p>
<p>To determine the spam score of the incoming emails, various parts of
the email will be analyzed including:</p>
<ul>
<li>SPF records, missing records or failing SPF will negatively impact
score</li>
<li>Email headers (will explain more in next section)</li>
<li>The language of subject and body of the message</li>
<li>Some filters also aggregate data to determine bad actors en mass for
lots of customers</li>
</ul>
<p>The last point is the one that can be really difficult to get around
if you run your own mail server. Specifically, entire IP ranges can get
blocked because too much spam was being sent; this will affect the
entire IP block’s ability to send email. Microsoft did <a
href="https://hostadvice.com/news/the-microsoft-outlook-network-still-rejects-emails-sent-from-linode-servers/">this</a>
to Linode, and was not very helpful in resolving the issue until much,
much later. And, even when they did fix it, it was only silently.</p>
<h2 id="dissecting-email-headers">Dissecting Email Headers</h2>
<p>Many spam filters look at email headers for information about the
emails to attempt to determine whether they are spam or not.
Unfortunately, email headers are roughly as readable as compiler output
and can look like a wall of information if you haven’t worked with them
before. Here is an example email header that we will be working
with:</p>
<div class="sourceCode" id="cb1"><pre
class="sourceCode email"><code class="sourceCode email"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a>From 010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com Fri Dec  6 07:30:08 2024</span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="bu">Delivered-To:</span> <span class="va">person@email.com</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a><span class="bu">Received:</span> by 2002:aa6:c561:0:b0:2ac:443d:529e with SMTP id z1csp807162lkp;</span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a>        Fri, 6 Dec 2024 04:30:09 -0800 <span class="co">(PST)</span></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a><span class="fu">X-Google-Smtp-Source: </span>AGHT+IGGzFj9rG8DAig1r5R5L5v4J0hhtWtnpjSTxXCTgWwNr2/+2Z/kUANsVLg/AwSqXtLz4Ksi</span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a><span class="fu">X-Received: </span>by 2002:a05:6214:5086:b0:6d8:9abb:3c28 with SMTP id 6a1803df08f44-6d8e71ad0abmr48222746d6.29.1733488208169;</span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a>        Fri, 06 Dec 2024 04:30:08 -0800 (PST)</span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a><span class="fu">ARC-Seal: </span>i=1; a=rsa-sha256; t=1733488208; cv=none;</span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true" tabindex="-1"></a>        d=google.com; s=arc-20240605;</span>
<span id="cb1-10"><a href="#cb1-10" aria-hidden="true" tabindex="-1"></a>        b=I1tzm9YUP3Ow+AtYga97dUCaUscPCFS4+Ghqgd0MPVNl/cUU8bWx5kWVtv1gLTNYot</span>
<span id="cb1-11"><a href="#cb1-11" aria-hidden="true" tabindex="-1"></a>         5NLiqCaNvxoXOwbYMGBtO/4nkoZ1Wml4Mtm1PyA3LsnjIUnCkdikjaevpXlSHzzr70/+</span>
<span id="cb1-12"><a href="#cb1-12" aria-hidden="true" tabindex="-1"></a>         Wf2EnzBykF0oGUyWGZKTZdvZgDHnh1aY9PZG5qEiIDT5YFmO86szXO5MGPfZSqHFDZxe</span>
<span id="cb1-13"><a href="#cb1-13" aria-hidden="true" tabindex="-1"></a>         TjKvhopvfhFH+Q6rEbXXemHjEYl1YG7IINGJ40DNVZpLwZzc7zIU9w8dbGZEbu8A7aRc</span>
<span id="cb1-14"><a href="#cb1-14" aria-hidden="true" tabindex="-1"></a>         VhIze0e0eYugkCHYBEO0R7x2mHtc1EHe9M18VaUP7HXV8ePLLB1/gJ9IUEA8WL0LauyT</span>
<span id="cb1-15"><a href="#cb1-15" aria-hidden="true" tabindex="-1"></a>         RWkQ==</span>
<span id="cb1-16"><a href="#cb1-16" aria-hidden="true" tabindex="-1"></a><span class="fu">ARC-Message-Signature: </span>i=1; a=rsa-sha256; c=relaxed/relaxed; d=google.com; s=arc-20240605;</span>
<span id="cb1-17"><a href="#cb1-17" aria-hidden="true" tabindex="-1"></a>        h=feedback-id:date:message-id:list-unsubscribe-post:list-unsubscribe</span>
<span id="cb1-18"><a href="#cb1-18" aria-hidden="true" tabindex="-1"></a>         :reply-to:subject:to:from:mime-version:dkim-signature:dkim-signature;</span>
<span id="cb1-19"><a href="#cb1-19" aria-hidden="true" tabindex="-1"></a>        bh=UbDnVmK+d3a5KWc8tc7jzr3yiQ1VTsl2vRyjB3oX5YE=;</span>
<span id="cb1-20"><a href="#cb1-20" aria-hidden="true" tabindex="-1"></a>        fh=cYhEvxUJVCrPuHhuhnYjrlZs1arjc/bcOjCL+MocS9Y=;</span>
<span id="cb1-21"><a href="#cb1-21" aria-hidden="true" tabindex="-1"></a>        b=U1t7XlEIb2HYPoHqDlfMkM5oynMZDQJKj56UN52F+ymAN5+GShWXIn3kyXdzgHhT4v</span>
<span id="cb1-22"><a href="#cb1-22" aria-hidden="true" tabindex="-1"></a>         GV/nlt075V17N+5/i+lOdUEnkjdQfGKWS11qtqLQhAYLFVWo5PApv5LdHolbPHuciM9w</span>
<span id="cb1-23"><a href="#cb1-23" aria-hidden="true" tabindex="-1"></a>         9bCxduC5qFsRBtmnR7JRg912F2yU9iml4vQxg5goUMPbJqtcv492ZHclERm+wWAM36f8</span>
<span id="cb1-24"><a href="#cb1-24" aria-hidden="true" tabindex="-1"></a>         39MqrjWuOjrSyu0+JhrJZeJio0t1f6PKppslQz7NM88LJUFtaha8QKyyjSZk1q0/KJr2</span>
<span id="cb1-25"><a href="#cb1-25" aria-hidden="true" tabindex="-1"></a>         dsrlqNkeaqVN/s6ri9V2QZU2upjaIj7MfGwtlVuCKm2D/eiufH5wYuku8aTxexLjsVKV</span>
<span id="cb1-26"><a href="#cb1-26" aria-hidden="true" tabindex="-1"></a>         N6lw==;</span>
<span id="cb1-27"><a href="#cb1-27" aria-hidden="true" tabindex="-1"></a>        dara=google.com</span>
<span id="cb1-28"><a href="#cb1-28" aria-hidden="true" tabindex="-1"></a><span class="fu">ARC-Authentication-Results: </span>i=1; mx.google.com;</span>
<span id="cb1-29"><a href="#cb1-29" aria-hidden="true" tabindex="-1"></a>       dkim=pass header.i=@duolingo.com header.s=nrqmk37y4yzwsm3xc6vb7hxfdn5ugmwd header.b=n3+GQSgE;</span>
<span id="cb1-30"><a href="#cb1-30" aria-hidden="true" tabindex="-1"></a>       dkim=pass header.i=@amazonses.com header.s=ug7nbtf4gccmlpwj322ax3p6ow6yfsug header.b=Hwmiz7tQ;</span>
<span id="cb1-31"><a href="#cb1-31" aria-hidden="true" tabindex="-1"></a>       spf=pass (google.com: domain of 010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com designates 54.240.10.133 as permitted sender) smtp.mailfrom=010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com;</span>
<span id="cb1-32"><a href="#cb1-32" aria-hidden="true" tabindex="-1"></a>       dmarc=pass (p=REJECT sp=REJECT dis=NONE) header.from=duolingo.com</span>
<span id="cb1-33"><a href="#cb1-33" aria-hidden="true" tabindex="-1"></a><span class="bu">Return-Path:</span> <span class="va">&lt;010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com&gt;</span></span>
<span id="cb1-34"><a href="#cb1-34" aria-hidden="true" tabindex="-1"></a><span class="bu">Received:</span> from a10-133.smtp-out.amazonses.com <span class="co">(a10-133.smtp-out.amazonses.com. [54.240.10.133])</span></span>
<span id="cb1-35"><a href="#cb1-35" aria-hidden="true" tabindex="-1"></a>        by mx.google.com with ESMTPS id 6a1803df08f44-6d8dac0949esi42739666d6.346.2024.12.06.04.30.07</span>
<span id="cb1-36"><a href="#cb1-36" aria-hidden="true" tabindex="-1"></a>        for <span class="va">&lt;person@email.com&gt;</span></span>
<span id="cb1-37"><a href="#cb1-37" aria-hidden="true" tabindex="-1"></a>        <span class="co">(version=TLS1_3 cipher=TLS_AES_128_GCM_SHA256 bits=128/128)</span>;</span>
<span id="cb1-38"><a href="#cb1-38" aria-hidden="true" tabindex="-1"></a>        Fri, 06 Dec 2024 04:30:08 -0800 <span class="co">(PST)</span></span>
<span id="cb1-39"><a href="#cb1-39" aria-hidden="true" tabindex="-1"></a><span class="bu">Received-SPF:</span> pass <span class="co">(google.com: domain of 010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com designates 54.240.10.133 as permitted sender)</span> client-ip=54.240.10.133;</span>
<span id="cb1-40"><a href="#cb1-40" aria-hidden="true" tabindex="-1"></a><span class="bu">Authentication-Results:</span> mx.google.com;</span>
<span id="cb1-41"><a href="#cb1-41" aria-hidden="true" tabindex="-1"></a>       dkim=pass header.i=@duolingo.com header.s=nrqmk37y4yzwsm3xc6vb7hxfdn5ugmwd header.b=n3+GQSgE;</span>
<span id="cb1-42"><a href="#cb1-42" aria-hidden="true" tabindex="-1"></a>       dkim=pass header.i=@amazonses.com header.s=ug7nbtf4gccmlpwj322ax3p6ow6yfsug header.b=Hwmiz7tQ;</span>
<span id="cb1-43"><a href="#cb1-43" aria-hidden="true" tabindex="-1"></a>       spf=pass <span class="co">(google.com: domain of 010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com designates 54.240.10.133 as permitted sender)</span> smtp.mailfrom=<span class="va">010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com</span>;</span>
<span id="cb1-44"><a href="#cb1-44" aria-hidden="true" tabindex="-1"></a>       dmarc=pass <span class="co">(p=REJECT sp=REJECT dis=NONE)</span> header.from=duolingo.com</span>
<span id="cb1-45"><a href="#cb1-45" aria-hidden="true" tabindex="-1"></a><span class="bu">DKIM-Signature:</span> v=1; a=rsa-sha256; q=dns/txt; c=relaxed/simple;</span>
<span id="cb1-46"><a href="#cb1-46" aria-hidden="true" tabindex="-1"></a>    s=nrqmk37y4yzwsm3xc6vb7hxfdn5ugmwd; d=duolingo.com; t=1733488207;</span>
<span id="cb1-47"><a href="#cb1-47" aria-hidden="true" tabindex="-1"></a>    h=Content-Type:MIME-Version:From:To:Subject:Reply-to:List-Unsubscribe:List-Unsubscribe-Post:Message-ID:Date;</span>
<span id="cb1-48"><a href="#cb1-48" aria-hidden="true" tabindex="-1"></a>    bh=UbDnVmK+d3a5KWc8tc7jzr3yiQ1VTsl2vRyjB3oX5YE=;</span>
<span id="cb1-49"><a href="#cb1-49" aria-hidden="true" tabindex="-1"></a>    b=n3+GQSgE3I9a8CN9baF2Qic3u+4XWRMKMtXumZpBi2x6E5W5LQbrz8JazcbP2Pk8</span>
<span id="cb1-50"><a href="#cb1-50" aria-hidden="true" tabindex="-1"></a>    stEVgA9hym6Uoj0PeVdjFKSXCxqLVw8R6dTyE3ab1Gvbx6A5j+IYUq1bkwLyG57YngQ</span>
<span id="cb1-51"><a href="#cb1-51" aria-hidden="true" tabindex="-1"></a>    oFX5uSARMoT7w5aKlM+0NcsF3tXXePz0b0L2TVRY=</span>
<span id="cb1-52"><a href="#cb1-52" aria-hidden="true" tabindex="-1"></a><span class="bu">DKIM-Signature:</span> v=1; a=rsa-sha256; q=dns/txt; c=relaxed/simple;</span>
<span id="cb1-53"><a href="#cb1-53" aria-hidden="true" tabindex="-1"></a>    s=ug7nbtf4gccmlpwj322ax3p6ow6yfsug; d=amazonses.com; t=1733488207;</span>
<span id="cb1-54"><a href="#cb1-54" aria-hidden="true" tabindex="-1"></a>    h=Content-Type:MIME-Version:From:To:Subject:Reply-to:List-Unsubscribe:List-Unsubscribe-Post:Message-ID:Date:Feedback-ID;</span>
<span id="cb1-55"><a href="#cb1-55" aria-hidden="true" tabindex="-1"></a>    bh=UbDnVmK+d3a5KWc8tc7jzr3yiQ1VTsl2vRyjB3oX5YE=;</span>
<span id="cb1-56"><a href="#cb1-56" aria-hidden="true" tabindex="-1"></a>    b=Hwmiz7tQUR/YIbVr4rhu2ecJ/d1KJdvvSqospSf7JElz8cRQAJSh+09Jyg38RJYO</span>
<span id="cb1-57"><a href="#cb1-57" aria-hidden="true" tabindex="-1"></a>    /e8yIMWvnLI+zsWD5wHZ+NGTKGWrOHyg4qr4Og06KAZyEgOeksu3nCahR6SfSBI2l0f</span>
<span id="cb1-58"><a href="#cb1-58" aria-hidden="true" tabindex="-1"></a>    PuxaO7Pu0EuR97IPfsLQQWb3Y9E5MZGxavfDA7Zw=</span>
<span id="cb1-59"><a href="#cb1-59" aria-hidden="true" tabindex="-1"></a><span class="bu">Content-Type:</span> multipart/alternative; boundary=<span class="st">&quot;===============0697970980562405415==&quot;</span></span>
<span id="cb1-60"><a href="#cb1-60" aria-hidden="true" tabindex="-1"></a><span class="bu">MIME-Version:</span> 1.0</span>
<span id="cb1-61"><a href="#cb1-61" aria-hidden="true" tabindex="-1"></a><span class="bu">From:</span> Duolingo <span class="va">&lt;no-reply@duolingo.com&gt;</span></span>
<span id="cb1-62"><a href="#cb1-62" aria-hidden="true" tabindex="-1"></a><span class="bu">To:</span> <span class="va">person@email.com</span></span>
<span id="cb1-63"><a href="#cb1-63" aria-hidden="true" tabindex="-1"></a><span class="bu">Subject:</span> =?utf-8?q?=F0=9F=98=B2_Your_Year_in_Review_is_here=2E?=</span>
<span id="cb1-64"><a href="#cb1-64" aria-hidden="true" tabindex="-1"></a><span class="bu">Reply-to:</span> Duolingo <span class="va">&lt;no-reply@duolingo.com&gt;</span></span>
<span id="cb1-65"><a href="#cb1-65" aria-hidden="true" tabindex="-1"></a><span class="bu">List-Unsubscribe:</span> &lt;https://blast.duolingo.com/web-redirect/200567?from_email=16715badf50216a7b0f662868d27190658681a0eImZhY2VwbGFudDM2QGdtYWlsLmNvbSI=&amp;user_id=cdcde602db5914665fd0d6917a97c8faa53d4f23NTMxMTU5OQ==&amp;email=16715badf50216a7b0f662868d27190658681a0eImZhY2VwbGFudDM2QGdtYWlsLmNvbSI=&amp;sample_id=8603&amp;target=aHR0cHM6Ly93d3cuZHVvbGluZ28uY29tL3Vuc3Vic2NyaWJlP3R5cGU9bm90aWZ5X2Fubm91bmNlbWVudCZlbWFpbD1bRW5jb2RlZEVtYWlsXSZ1dG1&gt;</span>
<span id="cb1-66"><a href="#cb1-66" aria-hidden="true" tabindex="-1"></a><span class="fu">List-Unsubscribe-Post: </span>List-Unsubscribe=One-Click</span>
<span id="cb1-67"><a href="#cb1-67" aria-hidden="true" tabindex="-1"></a><span class="bu">Message-ID:</span> <span class="va">&lt;010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@email.amazonses.com&gt;</span></span>
<span id="cb1-68"><a href="#cb1-68" aria-hidden="true" tabindex="-1"></a><span class="bu">Date:</span> Fri, 6 Dec 2024 12:30:07 +0000</span>
<span id="cb1-69"><a href="#cb1-69" aria-hidden="true" tabindex="-1"></a><span class="fu">Feedback-ID: </span>::1.us-east-1.RpxhJRmOpL41XzJPFX+GBBQj4+ioASSIVb8HK9KAN9A=:AmazonSES</span>
<span id="cb1-70"><a href="#cb1-70" aria-hidden="true" tabindex="-1"></a><span class="fu">X-SES-Outgoing: </span>2024.12.06-54.240.10.133</span></code></pre></div>
<p>Again, massive wall of barely parsable (by a human) text. So, let’s
look for our basic information and go from there.</p>
<div class="sourceCode" id="cb2"><pre
class="sourceCode email"><code class="sourceCode email"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a>From 010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com Fri Dec  6 07:30:08 2024</span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="bu">Delivered-To:</span> <span class="va">person@email.com</span></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="bu">Received:</span> by 2002:aa6:c561:0:b0:2ac:443d:529e with SMTP id z1csp807162lkp;</span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a>        Fri, 6 Dec 2024 04:30:09 -0800 <span class="co">(PST)</span></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a><span class="bu">Received:</span> from a10-133.smtp-out.amazonses.com <span class="co">(a10-133.smtp-out.amazonses.com. [54.240.10.133])</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a>        by mx.google.com with ESMTPS id 6a1803df08f44-6d8dac0949esi42739666d6.346.2024.12.06.04.30.07</span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a>        for <span class="va">&lt;person@email.com&gt;</span></span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true" tabindex="-1"></a>        <span class="co">(version=TLS1_3 cipher=TLS_AES_128_GCM_SHA256 bits=128/128)</span>;</span>
<span id="cb2-9"><a href="#cb2-9" aria-hidden="true" tabindex="-1"></a>        Fri, 06 Dec 2024 04:30:08 -0800 <span class="co">(PST)</span></span>
<span id="cb2-10"><a href="#cb2-10" aria-hidden="true" tabindex="-1"></a><span class="bu">Received-SPF:</span> pass <span class="co">(google.com: domain of 010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com designates 54.240.10.133 as permitted sender)</span> client-ip=54.240.10.133;</span>
<span id="cb2-11"><a href="#cb2-11" aria-hidden="true" tabindex="-1"></a><span class="bu">Authentication-Results:</span> mx.google.com;</span>
<span id="cb2-12"><a href="#cb2-12" aria-hidden="true" tabindex="-1"></a>       dkim=pass header.i=@duolingo.com header.s=nrqmk37y4yzwsm3xc6vb7hxfdn5ugmwd header.b=n3+GQSgE;</span>
<span id="cb2-13"><a href="#cb2-13" aria-hidden="true" tabindex="-1"></a>       dkim=pass header.i=@amazonses.com header.s=ug7nbtf4gccmlpwj322ax3p6ow6yfsug header.b=Hwmiz7tQ;</span>
<span id="cb2-14"><a href="#cb2-14" aria-hidden="true" tabindex="-1"></a>       spf=pass <span class="co">(google.com: domain of 010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com designates 54.240.10.133 as permitted sender)</span> smtp.mailfrom=<span class="va">010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com</span>;</span>
<span id="cb2-15"><a href="#cb2-15" aria-hidden="true" tabindex="-1"></a>       dmarc=pass <span class="co">(p=REJECT sp=REJECT dis=NONE)</span> header.from=duolingo.com</span>
<span id="cb2-16"><a href="#cb2-16" aria-hidden="true" tabindex="-1"></a><span class="bu">DKIM-Signature:</span> v=1; a=rsa-sha256; q=dns/txt; c=relaxed/simple;</span>
<span id="cb2-17"><a href="#cb2-17" aria-hidden="true" tabindex="-1"></a>    s=nrqmk37y4yzwsm3xc6vb7hxfdn5ugmwd; d=duolingo.com; t=1733488207;</span>
<span id="cb2-18"><a href="#cb2-18" aria-hidden="true" tabindex="-1"></a>    h=Content-Type:MIME-Version:From:To:Subject:Reply-to:List-Unsubscribe:List-Unsubscribe-Post:Message-ID:Date;</span>
<span id="cb2-19"><a href="#cb2-19" aria-hidden="true" tabindex="-1"></a>    bh=UbDnVmK+d3a5KWc8tc7jzr3yiQ1VTsl2vRyjB3oX5YE=;</span>
<span id="cb2-20"><a href="#cb2-20" aria-hidden="true" tabindex="-1"></a>    b=n3+GQSgE3I9a8CN9baF2Qic3u+4XWRMKMtXumZpBi2x6E5W5LQbrz8JazcbP2Pk8</span>
<span id="cb2-21"><a href="#cb2-21" aria-hidden="true" tabindex="-1"></a>    stEVgA9hym6Uoj0PeVdjFKSXCxqLVw8R6dTyE3ab1Gvbx6A5j+IYUq1bkwLyG57YngQ</span>
<span id="cb2-22"><a href="#cb2-22" aria-hidden="true" tabindex="-1"></a>    oFX5uSARMoT7w5aKlM+0NcsF3tXXePz0b0L2TVRY=</span>
<span id="cb2-23"><a href="#cb2-23" aria-hidden="true" tabindex="-1"></a><span class="bu">DKIM-Signature:</span> v=1; a=rsa-sha256; q=dns/txt; c=relaxed/simple;</span>
<span id="cb2-24"><a href="#cb2-24" aria-hidden="true" tabindex="-1"></a>    s=ug7nbtf4gccmlpwj322ax3p6ow6yfsug; d=amazonses.com; t=1733488207;</span>
<span id="cb2-25"><a href="#cb2-25" aria-hidden="true" tabindex="-1"></a>    h=Content-Type:MIME-Version:From:To:Subject:Reply-to:List-Unsubscribe:List-Unsubscribe-Post:Message-ID:Date:Feedback-ID;</span>
<span id="cb2-26"><a href="#cb2-26" aria-hidden="true" tabindex="-1"></a>    bh=UbDnVmK+d3a5KWc8tc7jzr3yiQ1VTsl2vRyjB3oX5YE=;</span>
<span id="cb2-27"><a href="#cb2-27" aria-hidden="true" tabindex="-1"></a>    b=Hwmiz7tQUR/YIbVr4rhu2ecJ/d1KJdvvSqospSf7JElz8cRQAJSh+09Jyg38RJYO</span>
<span id="cb2-28"><a href="#cb2-28" aria-hidden="true" tabindex="-1"></a>    /e8yIMWvnLI+zsWD5wHZ+NGTKGWrOHyg4qr4Og06KAZyEgOeksu3nCahR6SfSBI2l0f</span>
<span id="cb2-29"><a href="#cb2-29" aria-hidden="true" tabindex="-1"></a>    PuxaO7Pu0EuR97IPfsLQQWb3Y9E5MZGxavfDA7Zw=</span>
<span id="cb2-30"><a href="#cb2-30" aria-hidden="true" tabindex="-1"></a><span class="bu">From:</span> Duolingo <span class="va">&lt;no-reply@duolingo.com&gt;</span></span>
<span id="cb2-31"><a href="#cb2-31" aria-hidden="true" tabindex="-1"></a><span class="bu">To:</span> <span class="va">person@email.com</span></span>
<span id="cb2-32"><a href="#cb2-32" aria-hidden="true" tabindex="-1"></a><span class="bu">Subject:</span> =?utf-8?q?=F0=9F=98=B2_Your_Year_in_Review_is_here=2E?=</span>
<span id="cb2-33"><a href="#cb2-33" aria-hidden="true" tabindex="-1"></a><span class="bu">Reply-to:</span> Duolingo <span class="va">&lt;no-reply@duolingo.com&gt;</span></span>
<span id="cb2-34"><a href="#cb2-34" aria-hidden="true" tabindex="-1"></a><span class="bu">Message-ID:</span> <span class="va">&lt;010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@email.amazonses.com&gt;</span></span>
<span id="cb2-35"><a href="#cb2-35" aria-hidden="true" tabindex="-1"></a><span class="bu">Date:</span> Fri, 6 Dec 2024 12:30:07 +0000</span></code></pre></div>
<p>There is still a lot of information there, but it should look a bit
less intimidating and some of the fields may even look obvious. For
example, Delivered-To, is the email address that the email was delivered
too. The subject, is the subject of the email, etc. Some of the things
that might look slightly less straightforward are going to be things
like the <code>Received-SPF</code> and <code>DKIM-Signature</code>. In
the case of the <code>Received-SPF</code>:</p>
<div class="sourceCode" id="cb3"><pre
class="sourceCode email"><code class="sourceCode email"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="bu">Received-SPF:</span> pass <span class="co">(google.com: domain of 010001939bf2e76b-c528bc30-1748-4eba-9ca2-91507276373b-000000@bounces.duolingo.com designates 54.240.10.133 as permitted sender)</span> client-ip=54.240.10.133;</span></code></pre></div>
<p>We can tell that the message passed SPF because of the
<code>pass</code> toward the beginning of the line. That is auto echoed
in the section for <code>Authentication-Results</code>. That section
also shows that DKIM was passed as well, Then at the bottom of the
<code>Authentication-Results</code> we see the line
<code>dmarc=pass</code> then some more information on what the email is
supposed to do if DMARC is not passed. This means that this email has
passed the three main email authentication requirements. Past that, we
also get a specific time stamp the email was received, along with a
Message-ID to better track down the email if we need to look for it.</p>
<p>Great, but what was the other stuff that got removed? Some of it was
junk that doesn’t really affect anything. That is pretty much any line
that starts with <code>X-</code>. Those headers are not standardized,
and thus can kind of be made up by the email composer. The other
interesting section is the <code>ARC-Authentication-Results</code>. <a
href="https://en.wikipedia.org/wiki/Authenticated_Received_Chain">Authenticated
Received Chain (ARC)</a> is a method of validating email that could be
changed by a spam filter. Some spam filters will modify an email’s
headers, and this modification can cause the email to fail DKIM when it
gets through to the user. ARC fixes this by allowing intermediate
servers to sign off on the original validation results. This helps
prevent authentication issues, and increases deliverability.</p>
<h1 id="resources">Resources</h1>
<p>Email is a topic that has a lot happening all at once, and is not
really something for people just getting started with IT. It has a lot
of concepts that you really should know before jumping in, and there are
a lot of things that must be done correctly for it to actually work.
Unfortunately, it is also a moving target. What works today, may not
work tomorrow if the big mail providers change their mind on something.
This blog post is not intended to be an in-depth tutorial on anything,
rather, just a primer to get familiar with some of the different aspects
of email and a general idea of how they fit together. If you are
interested in running your own mail server, I have included some
resources that I highly recommend checking out before doing so:</p>
<ul>
<li><a
href="https://dataswamp.org/~solene/2024-07-24-openbsd-email-server-setup.html">Solene:
Setting Up Open BSD as an Email Server</a></li>
<li><a href="https://www.youtube.com/watch?v=o_xFQ2JsWFQ">Michael W
Lucas: The State of Email NYC*BUG</a></li>
<li><a href="https://mwl.io/nonfiction/tools#ryoms">Michael W Lucas: Run
Your Own Mail Server</a></li>
<li><a
href="https://www.mailercheck.com/articles/how-to-read-and-understand-email-headers">MailerCheck:
How to Read and Understand Email Headers</a></li>
<li><a href="https://mxtoolbox.com/EmailHeaders.aspx">MXToolbox Email
Header Parser</a></li>
</ul>
</article>
<footer>
    <p>
        Comments or suggestions?<br />
        Contact me: <a href="mailto:tyler.clark@foxide.xyz">tyler.clark@foxide.xyz</a>
    </p>
</footer>
