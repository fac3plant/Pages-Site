<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="image/png" href="favicon.png" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
        <title>Tyler's website</title>
<meta property="og:title" content="Setting up CryptPad for Self-hosted document and calendars" />
      <meta property="og:description" content="" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="/projects/2023-10-07" />
      <meta property="og:image" content="https://foxide.xyz/" />

    </head>

    <body>
        <header>
            <h1>Tyler's Site</h1>
        </header>
        <nav>
            <a href="https://foxide.xyz/">home</a>&emsp;
            <a href="https://foxide.xyz/articles.html">articles</a>&emsp;
            <a href="https://foxide.xyz/projects.html ">projects</a>&emsp;
            <a href="https://foxide.xyz/consulting.html">consulting</a>&emsp;
            <a href="https://git.foxide.xyz">code</a>
        </nav>
        <article>
<h1 id="abstract">Abstract</h1>
<p>“CryptPad is a collaborative office suite that is end-to-end
encrypted and open-source.” - <a href="https://cryptpad.org/">CryptPad’s
website</a></p>
<p>I’ve been hosting a CryptPad instance for a few years, but have been
using it a lot more recently for things like notes and calendaring as it
is easier than keeping everything synced between devices. It operates a
lot like Office 365 or Google Drive’s office suite, however, CryptPad is
self-hosted, open source, and puts privacy first.</p>
<p>Setting up a CryptPad instance is not particularly difficult
depending on what your security concerns are. For example, my original
CryptPad instance was self-hosted on my LAN inside of a FreeBSD jail.
The concern for security and was less than if my instance would have
been public, so, I just got it working rather than configuring it to
production quality. However, for a public instance, there is a lot more
tweaking involved to make sure the server itself is secure and has as
few holes as possible for attackers. In this post I will be going over
my experience with both an “insecure” setup and a “production” setup.
The insecure setup is not recommended, but might make the most sense for
someone only concerned about having access to it on their LAN rather
than across the Internet. For a public CryptPad instance available on
the Internet, please <strong>do your research</strong>. CryptPad has a
very good <a
href="https://docs.cryptpad.org/en/admin_guide/installation.html">guide</a>
on setting up a public instance for CryptPad, before trying anything in
this blog post, start there. This post is more so recounting some issues
that I had (that were likely self-imposed).</p>
<h1 id="insecure-cryptpad-setup">Insecure CryptPad setup</h1>
<p>My “insecure” CryptPad instance is running inside of a FreeBSD jail,
so all of the commands will assume FreeBSD. However, this should not be
terribly difficult to translate into Linux (s/pkg/apt/g and
s//usr/local/<span class="math inline">${dir}/\/$</span>{dir}/g should
suffice in most cases). When setting up any new server, making sure
everything is up-to-date is highly important. After that, install Nginx,
node20, npm-node20, and git.</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Run the following as root</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="ex">pkg</span> update <span class="kw">&amp;&amp;</span> <span class="ex">pkg</span> upgrade</span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a><span class="ex">pkg</span> install <span class="at">-y</span> git nginx node20 npm-node20</span></code></pre></div>
<p>Note: Nginx is the recommended web server for use with CryptPad.
Other web servers will work, but the documentation will references Nginx
configs which might make getting the setup complete easier</p>
<p>Then download CryptPad and install dependencies.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> clone https://github.com/cryptpad/cryptpad.git cryptpad <span class="kw">&amp;&amp;</span> <span class="bu">cd</span> cryptpad</span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="ex">npm</span> install</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="ex">npm</span> run install:components</span></code></pre></div>
<p>From here, copy the example config file by running
<code>cp config/config.example.js config/config.js</code> then run
<code>node server</code> to start CryptPad. While CryptPad will
technically run like this, it will not actually be useful until
modifying <code>config/config.js</code>. Within the config file, there
are some variables that must be changed for CryptPad to actually be
accessible from other machines on the network. The first set of
variables that must be changed are: <code>httpUnsafeOrigin</code> and
<code>httpSafeOrigin</code>. What these variables do is not particularly
important in this section, however, will become much more important
during the “Production” section of this post. For now, just set the
variable to whatever domain name that the instance will be used from
(i.g. localdocs.internal). The next configuration item that must be
changed is <code>httpAddress</code>. By default it only listens on
localhost and should be changed to whatever the IP address of the server
is going to be. Alternatively, it can listen on all ports, but that
creates much more attack surface on the server than necessary. All of
the other defaults should be fine to get CryptPad working; though it is
worth reading through the different configuration options available.</p>
<p>Next we configure the web server to be able to proxy the correct
ports. Nginx has some configuration examples in the default config file,
but it is just as easy to add another server block to do the proxy. The
following example assumes that the domain name being used for the
CryptPad instance is ‘localdocs.internal’, the IP address of the server
is 192.168.1.100, and that the ‘httpPort’ in CryptPad’s config file has
remained unchanged.</p>
<pre class="config"><code>server {
    listen: 80;
    server_name localdocs.internal;

    location / {
        proxy_pass http://192.168.1.100:3000;
    }

# If this is not set, then CryptPad will not load properly,
# and the application will be unusable
    location = /cryptpad_websocket {
        proxy_pass http://localhost:3000;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection upgrade;
    }

}</code></pre>
<p>Then just make sure that Nginx is enabled by adding
<code>nginx_enable="YES"</code> to /etc/rc.conf, then start by running
<code>service nginx start</code> as root. From there, run
<code>node server</code> in the cryptpad directory, then attempt to
visit the page at localdocs.internal. Assuming it loads, the last thing
to do is to make CryptPad run as a daemon. This can be done by using the
<code>daemon</code> utility in FreeBSD, or by using the <a
href="https://github.com/cryptpad/cryptpad/blob/main/docs/rc.d-cryptpad">example</a>
rc.d file to create a service.</p>
<h1 id="production-cryptpad-setup">Production CryptPad setup</h1>
<p>The production instance is similar to the insecure instance, however,
you will also need to have a domain with an SSL certificate. I used <a
href="https://certbot.eff.org/">certbot</a> for this as it is quick and
easy while also costing nothing (Go EFF!). Also for this, we must have
two domains (at least subdomains) to help prevent XSS attacks on the
CryptPad instance. This section of the blog post will be in more general
terms as there is quite a bit of information that will vary from
instance to instance, plus the example Nginx config does quite a good
job of documenting what everything does. The first thing to do is to
make sure that each of the domain names is registered so that they point
to the server, and thus can get an SSL cert.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="co"># All commands must be run as root</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a><span class="co"># Install certbot</span></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a><span class="ex">pkg</span> install py-certbot-nginx</span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a><span class="co"># Then get the certificates for the domains</span></span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a><span class="ex">certbot</span> <span class="at">--nginx</span></span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true" tabindex="-1"></a><span class="co"># Alternatively you can just get the certs,</span></span>
<span id="cb4-8"><a href="#cb4-8" aria-hidden="true" tabindex="-1"></a><span class="co"># but it will be more work</span></span>
<span id="cb4-9"><a href="#cb4-9" aria-hidden="true" tabindex="-1"></a><span class="ex">certbot</span> certonly <span class="at">--nginx</span></span></code></pre></div>
<p>The next step is to copy the example Nginx config file into the
current Nginx config file so it can be modified to fit the instance we
are trying to set up. While there are likely 100 ways to do this in
FreeBSD, the way that made sense to me was to fetch the <a
href="https://raw.githubusercontent.com/cryptpad/cryptpad/main/docs/example.nginx.conf">file</a>,
then as root run
<code>cat example.nginx.conf &gt;&gt; /usr/local/etc/nginx/nginx.conf</code>.
This command will copy the example.nginx.conf file to the end of the
current nginx config file. It can then be opened in ${EDITOR} and the
server block for CryptPad can be moved to a more suitable location and
the editing process can begin. Again, this file will vary from person to
person a bit, and there will be a bit of trial and error to get the
configuration correct. The ‘main_domain’ is the domain (or subdomain)
that the site will be accessed from. The ‘sandbox_domain’ is a
secondairy domain that is used strictly as a security measure against
XSS attacks. There will also be some SSL items that will have to be
commented out as the example file has some SSL configuration that should
have been handled already if you installed your certs with it.</p>
<p>Once nginx is configured, enabled, and started, we can then start
working with CryptPad itself to get it configured and working. First, as
in the other section of this post, copy the
<code>config.example.js</code> to <code>config.js</code> in the
<code>config</code> folder in CryptPad. Then open <code>config.js</code>
in ${EDITOR} and begin configuring it for your instance. Change
<code>httpUnsafeOrigin</code> to the same as the
<code>main_domain</code> in the Nginx config file, change
<code>httpSafeOrigin</code> to the same as the
<code>sandbox_domain</code> in the Nginx config file. Most everything
else can be left as default, however, it is worth a look at the rest of
the config file as there are some other useful configuration items in
there. From there, run <code>node server</code> and attempt to access
the page. If it loads and you are able to login and access the drive or
another app, the configuration works. Now just use the <a
href="https://github.com/cryptpad/cryptpad/blob/main/docs/rc.d-cryptpad">rc.d
script</a> to make CryptPad a service and manage it as such.</p>
<h1 id="more-documentation">More Documentation</h1>
<p>CryptPad is a very well documented project with a lot of helpful
resources to get started. Because of that, if you are interested in
setting up your own public instance, I recommend starting there rather
than this documentation. These pages are good for starting out with
CryptPad and asking for help.</p>
<ul>
<li><a href="https://docs.cryptpad.org/en/user_guide/index.html">Basic
User Guide</a></li>
<li><a
href="https://docs.cryptpad.org/en/admin_guide/installation.html">Installation
Documentation</a></li>
<li><a href="https://forum.cryptpad.org/">CryptPad Support
Forum</a></li>
</ul>
<!--  LocalWords:  CryptPad CryptPad's config configs FreeBSD Nginx
 -->
</article>
<footer>
    <p>
        Comments or suggestions?<br />
        Contact me: <a href="mailto:tyler.clark@foxide.xyz">tyler.clark@foxide.xyz</a>
    </p>
</footer>
