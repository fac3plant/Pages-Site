<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="image/png" href="favicon.png" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
        <title>Tyler's website</title>
<meta property="og:title" content="MFA for SSH sessions" />
      <meta property="og:description" content="" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="/projects/2023-07-18" />
      <meta property="og:image" content="https://foxide.xyz/" />

    </head>

    <body>
        <header>
            <h1>Tyler's Site</h1>
        </header>
        <nav>
            <a href="https://foxide.xyz/">home</a>&emsp;
            <a href="https://foxide.xyz/articles.html">articles</a>&emsp;
            <a href="https://foxide.xyz/projects.html ">projects</a>&emsp;
            <a href="https://foxide.xyz/consulting.html">consulting</a>&emsp;
            <a href="https://git.foxide.xyz">code</a>
        </nav>
        <article>
<h1 id="abstract">Abstract</h1>
<p>I wanted to research MFA methods for SSH connections that did not
require a password. Passwords, as many people know, kind of suck.
However, authentication is needed for SSH to work properly. How can you
authenticate for SSH? Easy an RSA key; and RSA keys are pretty secure,
especially if they are encrypted with a password. What if you are
wanting more though? You <em>could</em> use a password for the user
account as well, but again, passwords suck and we already have one for
our RSA key. So what about One Time Passwords (OTPs)? Specifically TOTPs
like you would get by using an app like <a
href="https://authy.com/">Authy</a> or <a
href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">Google
Authenticator</a>. That would be great! This is a guide on how to do
just that with FreeBSD 13.2 and OpenSSH 9.3.</p>
<h1 id="use-case">Use Case</h1>
<p>The use case for this is probably pretty niche. For most people RSA
keys (especially encrypted ones) will suffice for security with SSH
sessions. People that are in need of more authentication than that
likely have standards bodies to comply with; and I do not believe they
would consult this blog for advise on how to configure authentication to
their machines. However, if MFA with an RSA key and TOTP is interesting
to you, here is how I went about doing it.</p>
<h1 id="openssh-server">OpenSSH Server</h1>
<p>I have OpenSSH 9.3 installed on the server that has this
configuration.Other OpenSSH versions will likely work, however, be
mindful in the differences between OpenSSH 9.3 and the version the
reader may be using.</p>
<h2 id="generating-keys">Generating Keys</h2>
<p>Many people using SSH will already have keys for logging into their
servers, however, for the sake of completeness the
<code>ssh-keygen</code> command is how one would go about generating key
pairs.</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> ssh-keygen <span class="at">-t</span> rsa</span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="op">&gt;</span> Enter <span class="ex">passphrase</span> <span class="er">(</span><span class="ex">empty</span> for no passphrase<span class="kw">)</span><span class="bu">:</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a><span class="op">&gt;</span> Enter <span class="ex">same</span> passphrase again:</span></code></pre></div>
<p>The <code>ssh-keygen</code> command will generate an asymetrical key
pair. The default location for these files are
<code>${HOME}/.ssh/</code> and they will be called <code>id_rsa</code>
and <code>id_rsa.pub</code>. The contents of the .pub file will need to
go on the remote server the file
<code>${HOME}/.ssh/authorized_keys</code>. This fill is the default file
OpenSSH will be looking at to see if a key will authenticate a user to
allow a successful login. The <code>id_rsa</code> file will need to be
kept safe as it will authenticate to anything that corresponds to the
.pub key that was generated with it. Alternatively the
<code>id_rsa.pub</code> key can be shared with anyone and everyone; this
file contains nothing sensitive.</p>
<p>Please note that while it is possible to not have a password for a
key pair, it is a bad idea. The password (or passphrase as ssh-keygen
calls it) will encrypt the key so that it cannot be used without knowing
the password. It is also worth mentioning that there are other key pairs
that <code>ssh-keygen</code> will generate. For more documentation on
the utility please reference the <a
href="https://man.freebsd.org/cgi/man.cgi?ssh-keygen">ssh-keygen man
page</a>.</p>
<h2 id="openssh-server-config">OpenSSH Server Config</h2>
<p>Below is my OpenSSH server config with some comments removed. All of
the comments in the config below were added during the time of writing
to help explain some options better.</p>
<pre class="config"><code># Forces v2 protocol
Protocol 2
PermitRootLogin no
MaxAuthTries 5
PubkeyAuthentication yes

# Forces public key authentication, then allows for keyboard authentication
# the keyboard authentication needs to be enabled to allow entry of the TOTP
AuthenticationMethods publickey,keyboard-interactive
ChallengeResponseAuthentication yes
KbdInteractiveAuthentication yes
=======
X11Forwarding no
ClientAliveInterval 300
Subsystem       sftp    /usr/libexec/sftp-server</code></pre>
<p>It is always a good idea to keep a backup terminal open to the remote
server just in case. If something is configured incorrectly and a backup
session is not open then you will have a difficult time getting back
into the remote server. Restart the sshd service for the changes to take
effect.</p>
<pre><code>$ sudo service sshd restart</code></pre>
<p>and attempt to login to the remote server. Rather than asking for a
password from the SSH session, it should prompt for a password for the
rsa key pair (you did set a password for that right?) and let you in
once the key is unlocked.</p>
<h1 id="google-authenticator-libpam">Google Authenticator LibPam</h1>
<p>The next part is the part that I found a bit more tricky as I am not
as good with PAM configuration. However, there are a lot of resources
online about it. I used <a
href="https://docs.freebsd.org/en/articles/pam/">this</a> page from the
FreeBSD handbook as a guide to navigate PAM. For a more in-depth guide
to PAM Michael W Lucas sells a <a
href="https://www.amazon.com/dp/1537657704">book</a> about masting PAM.
He also has a talk on PAM available <a
href="https://www.youtube.com/watch?v=Mc2p6sx2s7k">here</a>.</p>
<h2 id="download-and-install-google-authenticator-libpam">Download and
install Google Authenticator LibPam</h2>
<p>The <a
href="https://github.com/google/google-authenticator-libpam">Github
page</a> for Google Authenticator LibPAM is well documented and has a
lot more than I am going to cover in this blog post; for further
information about configuring this particular module, go there.</p>
<p>Download and install the PAM module.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git clone https://github.com/google/google-authenticator-libpam</span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> ./bootstrap.sh</span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> ./configure</span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> make</span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> sudo make install</span></code></pre></div>
<p>Then configure PAM to use it for sshd. Below is the file located in
<code>/etc/pam.d/sshd</code> which handles authentication for OpenSSH
server.</p>
<pre class="config"><code>auth            required        pam_google_authenticator.so nullok
auth            required        pam_opie.so             no_warn no_fake_prompts
auth            required        pam_opieaccess.so       no_warn allow_local

account         required        pam_nologin.so
account         required        pam_login_access.so

session         required        pam_permit.so
</code></pre>
<p>I do not have enough knowledge about PAM to go through this
configuration and explain what each line does in depth. For that I am
going to defer to the resources outlined at the top of this blog
post.</p>
<h1 id="totp-app">TOTP App</h1>
<p>Finally we are ready to set up our TOTP app. Run the command
<code>google-authenticator</code>, if you have ‘libqrencode’ installed
it will output a QR code that can be scanned on your app. If
‘libqrencoe’ is not installed, you can follow the link or manually enter
the secret (scanning the QR code is by far the easiest). Once done you
should have MFA setup for SSH sessions on remote machines.</p>
<h1 id="other-ideas">Other Ideas</h1>
<p>While logging into a remote machine is great, this blog post could
also be modified to do other neat things to reduce or even eliminate
passwords. For example, on the machine I set this up on, I also changed
PAM’s entry for doas to allow using TOTP instead of typing the password
for the user. Doing this could theoretically allow me to disable the
user’s password entirely as they would not be using it for anything.</p>
</article>
<footer>
    <p>
        Comments or suggestions?<br />
        Contact me: <a href="mailto:tyler.clark@foxide.xyz">tyler.clark@foxide.xyz</a>
    </p>
</footer>
