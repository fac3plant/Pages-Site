<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="image/png" href="favicon.png" />
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="../style.css" />
        <title>Tyler's website</title>
<meta property="og:title" content="Basics of Building and Patching Software" />
      <meta property="og:description" content="" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="/projects/2024-11-22" />
      <meta property="og:image" content="https://foxide.xyz/" />

    </head>

    <body>
        <header>
            <h1>Tyler's Site</h1>
        </header>
        <nav>
            <a href="https://foxide.xyz/">home</a>&emsp;
            <a href="https://foxide.xyz/articles.html">articles</a>&emsp;
            <a href="https://foxide.xyz/projects.html ">projects</a>&emsp;
            <a href="https://foxide.xyz/consulting.html">consulting</a>&emsp;
            <a href="https://git.foxide.xyz">code</a>
        </nav>
        <article>
<h1 id="abstract">Abstract</h1>
<p>Modern Unix-like operating systems have a variety of ways of getting
software. The most common method is probably the package manager of the
operating system you are using; however, some other options are:</p>
<ul>
<li>Containers, such as Snaps, Flatpaks, or AppImages</li>
<li>Build recipes, such as ports for the BSDs or the AUR for Arch
Linux</li>
<li>Downloading the binaries directly, such as with Chrome</li>
</ul>
<p>In addition to those above options, there is the option to build from
source (at least with software the source is available). While doing
this may not be extremely useful for many desktop Linux users, many jobs
that involve working with Linux will want some experience in building
(and maybe patching) software to run on the system. This article is a
quick introduction into some common ways to get , compile, and patch
source code. It also aims to cover the logic behind each of these steps
for the case that they <strong>WILL</strong> fail, as building software
from source is generally not a something that can be copy and pasted
from one application’s source to another. As an example for this write
up, I am going to be using a fairly simple project that is relatively
easy to build. The project is a redshifting program called <a
href="https://github.com/faf0/sct">SCT</a>, and uses a bit of C to
change the color temperature of your screen.</p>
<p>I also want to go ahead and thank my co-worker, William, for helping
proof-read this post ahead of time as he pointed out some technical
issues that I did not initially catch; specifically, that I should
mention the required dependencies for SCT to compile and take some
guesswork out, as well as some vague instructions towards the beginning
of the post.</p>
<h1 id="obtaining-source-code">Obtaining Source Code</h1>
<p>The first hurdle in dealing with source code is actually obtaining
it. As far as the ‘how’, it really depends on what you are trying to
build. That being said, many projects have either a read-only or an
unofficial mirror on sites like GitHub.</p>
<p>For this example, there are two ways to go about getting the source
code. Option 1: Click on the ‘Code’ button towards the top right of the
GitHub page, then click ‘Download ZIP’ at the bottom of the small dialog
window, or Option 2: use <code>git</code> to download the software. Both
options work, however, using <code>git</code> will allow you to keep
track of software changes (future updates, or custom patches added by
you) easier than the ZIP file will.</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="co"># One option for opening the ZIP archieve if that was the chosen method</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="ex">7z</span> x sct-master.zip</span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a><span class="co"># Alternatively, the command if git was used</span></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> clone https://github.com/faf0/sct</span></code></pre></div>
<p>From there, simply <code>cd</code> into the <code>sct</code>
directory and we can begin the next step.</p>
<h1 id="building-the-software">Building the software</h1>
<p>Before building the software, we need to make sure we have all the
required dependencies; I am not going to list the command to install the
required dependencies for all Linux distros ever, but I will provide
what you would need for Debian/Ubuntu/Mint. If you do not use one of
those, you can search for similar package names in your package manager
of choice and you should find it. Debian also does not, at least by
default, include a compiler or <code>make</code>, so that has to be
installed as well.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">apt</span> install <span class="at">-y</span> libx11-dev libxrandr-dev make gcc</span></code></pre></div>
<p>Now that we have the required libraries, we have to compile (or
build) the software. This step is where things can vary heavily
depending on a lot of different factors such as build system, language
used, dependencies, target operating system, etc. Most projects will
have instructions in their <code>README</code> file on what the
dependencies are and roughly how to build it. However, it is important
to read it very carefully as minor details can end up being the
difference between successfully compiled software and hours of
incredibly frustrating troubleshooting as to what is causing it to
SEGFAULT. <code>SCT</code> happens to be a fairly easy software to build
and can simply be built with <code>make</code> in the project directory.
Once the software is built, it can be installed by running
<code>make install</code> (again in the project directory). Wonderful,
but what did we learn? Really nothing… So, let’s backup, what is
<code>make</code> and why does it install stuff on my system?</p>
<h2 id="the-wonderful-world-of-makefiles">The wonderful world of
Makefiles</h2>
<p><a href="https://en.wikipedia.org/wiki/Make_(software)">Make</a> is a
command line tool that is often used to automate building software,
although it is not limited to just building software. Going over the
ins-and-outs of all the things <code>make</code> can do is well outside
the scope of this one blog post, however, if the reader is interested in
learning more about the topic the <a
href="https://www.gnu.org/software/make/manual/make.html">GNU Make man
page</a> is a great place to start. This post will go into some general
ideas to get started with reading a basic <code>Makefile</code>
(configuration file for what <code>make</code> will do when
invoked).</p>
<p>Getting back to the actually getting code compiled, let’s begin by
looking at what the manual compilation command looks like, as that is
what <code>make</code> will end up doing:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="fu">gcc</span> <span class="at">-Wall</span> <span class="at">-Wextra</span> <span class="at">-Werror</span> <span class="at">-pedantic</span> <span class="at">-std</span><span class="op">=</span>c99 <span class="at">-O2</span> <span class="at">-I</span> /usr/X11R6/include xsct.c <span class="at">-o</span> xsct <span class="at">-L</span> /usr/X11R6/lib <span class="at">-lX11</span> <span class="at">-lXrandr</span> <span class="at">-lm</span> <span class="at">-s</span></span></code></pre></div>
<p>That looks super annoying to type each time you want to compile
something; thankfully <code>make</code> can automate that. Let’s look at
the <code>Makefile</code> to see what is going on in there:</p>
<div class="sourceCode" id="cb4"><pre
class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="dt">CC</span> <span class="ch">?=</span><span class="st"> gcc</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="dt">CFLAGS</span> <span class="ch">?=</span><span class="st"> -Wall -Wextra -Werror -pedantic -std=c99 -O2 -I /usr/X11R6/include</span></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a><span class="dt">LDFLAGS</span> <span class="ch">?=</span><span class="st"> -L /usr/X11R6/lib -s</span></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a><span class="dt">PREFIX</span> <span class="ch">?=</span><span class="st"> /usr</span></span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a><span class="dt">BIN</span> <span class="ch">?=</span><span class="st"> </span><span class="ch">$(</span><span class="dt">PREFIX</span><span class="ch">)</span><span class="st">/bin</span></span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a><span class="dt">MAN</span> <span class="ch">?=</span><span class="st"> </span><span class="ch">$(</span><span class="dt">PREFIX</span><span class="ch">)</span><span class="st">/share/man/man1</span></span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true" tabindex="-1"></a><span class="dt">INSTALL</span> <span class="ch">?=</span><span class="st"> install</span></span>
<span id="cb4-8"><a href="#cb4-8" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-9"><a href="#cb4-9" aria-hidden="true" tabindex="-1"></a><span class="dt">PROG</span> <span class="ch">=</span><span class="st"> xsct</span></span>
<span id="cb4-10"><a href="#cb4-10" aria-hidden="true" tabindex="-1"></a><span class="dt">SRCS</span> <span class="ch">=</span><span class="st"> src/xsct.c</span></span>
<span id="cb4-11"><a href="#cb4-11" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-12"><a href="#cb4-12" aria-hidden="true" tabindex="-1"></a><span class="dt">LIBS</span> <span class="ch">=</span><span class="st"> -lX11 -lXrandr -lm</span></span>
<span id="cb4-13"><a href="#cb4-13" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-14"><a href="#cb4-14" aria-hidden="true" tabindex="-1"></a><span class="dv">$(PROG):</span><span class="dt"> </span><span class="ch">$(</span><span class="dt">SRCS</span><span class="ch">)</span></span>
<span id="cb4-15"><a href="#cb4-15" aria-hidden="true" tabindex="-1"></a><span class="er">    </span><span class="ch">$(</span><span class="dt">CC</span><span class="ch">)</span> <span class="ch">$(</span><span class="dt">CFLAGS</span><span class="ch">)</span> <span class="ch">$^</span> -o <span class="ch">$@</span> <span class="ch">$(</span><span class="dt">LDFLAGS</span><span class="ch">)</span> <span class="ch">$(</span><span class="dt">LIBS</span><span class="ch">)</span></span>
<span id="cb4-16"><a href="#cb4-16" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-17"><a href="#cb4-17" aria-hidden="true" tabindex="-1"></a><span class="dv">install:</span><span class="dt"> </span><span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span><span class="dt"> </span><span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span><span class="dt">.1</span></span>
<span id="cb4-18"><a href="#cb4-18" aria-hidden="true" tabindex="-1"></a><span class="er">    </span><span class="ch">$(</span><span class="dt">INSTALL</span><span class="ch">)</span> -d <span class="ch">$(</span><span class="dt">DESTDIR</span><span class="ch">)$(</span><span class="dt">BIN</span><span class="ch">)</span></span>
<span id="cb4-19"><a href="#cb4-19" aria-hidden="true" tabindex="-1"></a>    <span class="ch">$(</span><span class="dt">INSTALL</span><span class="ch">)</span> -m 0755 <span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span> <span class="ch">$(</span><span class="dt">DESTDIR</span><span class="ch">)$(</span><span class="dt">BIN</span><span class="ch">)</span></span>
<span id="cb4-20"><a href="#cb4-20" aria-hidden="true" tabindex="-1"></a>    <span class="ch">$(</span><span class="dt">INSTALL</span><span class="ch">)</span> -d <span class="ch">$(</span><span class="dt">DESTDIR</span><span class="ch">)$(</span><span class="dt">MAN</span><span class="ch">)</span></span>
<span id="cb4-21"><a href="#cb4-21" aria-hidden="true" tabindex="-1"></a>    <span class="ch">$(</span><span class="dt">INSTALL</span><span class="ch">)</span> -m 0644 <span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span>.1 <span class="ch">$(</span><span class="dt">DESTDIR</span><span class="ch">)$(</span><span class="dt">MAN</span><span class="ch">)</span></span>
<span id="cb4-22"><a href="#cb4-22" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-23"><a href="#cb4-23" aria-hidden="true" tabindex="-1"></a><span class="dv">uninstall:</span></span>
<span id="cb4-24"><a href="#cb4-24" aria-hidden="true" tabindex="-1"></a><span class="er">    </span>rm -f <span class="ch">$(</span><span class="dt">BIN</span><span class="ch">)</span>/<span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span></span>
<span id="cb4-25"><a href="#cb4-25" aria-hidden="true" tabindex="-1"></a>    rm -f <span class="ch">$(</span><span class="dt">MAN</span><span class="ch">)</span>/<span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span>.1</span>
<span id="cb4-26"><a href="#cb4-26" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-27"><a href="#cb4-27" aria-hidden="true" tabindex="-1"></a><span class="dv">clean:</span></span>
<span id="cb4-28"><a href="#cb4-28" aria-hidden="true" tabindex="-1"></a><span class="er">    </span>rm -f <span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span></span></code></pre></div>
<p>Anyone familiar with shell scripting may be able read and kind of see
what the file is doing. However, not everyone is already used to shell
scripting, so let’s add some comments to make it a bit more clear for
those that are not as familiar:</p>
<div class="sourceCode" id="cb5"><pre
class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="co"># This section is just setting variables that will be used later</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a><span class="co"># Variables set with just &#39;?=&#39; are conditional variables</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a><span class="co"># that only matter if the variable is not already set by the environment.</span></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a><span class="co"># variables set with &#39;=&#39; are ones that will be used verbatim</span></span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true" tabindex="-1"></a><span class="co"># This variable is just setting the compiler, gcc in this case</span></span>
<span id="cb5-7"><a href="#cb5-7" aria-hidden="true" tabindex="-1"></a><span class="dt">CC</span> <span class="ch">?=</span><span class="st"> gcc</span></span>
<span id="cb5-8"><a href="#cb5-8" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-9"><a href="#cb5-9" aria-hidden="true" tabindex="-1"></a><span class="co"># This variable is setting some C compilation flags</span></span>
<span id="cb5-10"><a href="#cb5-10" aria-hidden="true" tabindex="-1"></a><span class="dt">CFLAGS</span> <span class="ch">?=</span><span class="st"> -Wall -Wextra -Werror -pedantic -std=c99 -O2 -I /usr/X11R6/include</span></span>
<span id="cb5-11"><a href="#cb5-11" aria-hidden="true" tabindex="-1"></a><span class="dt">LDFLAGS</span> <span class="ch">?=</span><span class="st"> -L /usr/X11R6/lib -s</span></span>
<span id="cb5-12"><a href="#cb5-12" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-13"><a href="#cb5-13" aria-hidden="true" tabindex="-1"></a><span class="co"># This variable is setting the prefix for where things should be installed</span></span>
<span id="cb5-14"><a href="#cb5-14" aria-hidden="true" tabindex="-1"></a><span class="co"># This doesn&#39;t /really/ matter, but on BSD systems, would probably</span></span>
<span id="cb5-15"><a href="#cb5-15" aria-hidden="true" tabindex="-1"></a><span class="co"># be changed to `/usr/local`</span></span>
<span id="cb5-16"><a href="#cb5-16" aria-hidden="true" tabindex="-1"></a><span class="dt">PREFIX</span> <span class="ch">?=</span><span class="st"> /usr</span></span>
<span id="cb5-17"><a href="#cb5-17" aria-hidden="true" tabindex="-1"></a><span class="co"># The $(PREFIX) is how the variable can be recalled</span></span>
<span id="cb5-18"><a href="#cb5-18" aria-hidden="true" tabindex="-1"></a><span class="co"># In this case, it is just being called while setting another</span></span>
<span id="cb5-19"><a href="#cb5-19" aria-hidden="true" tabindex="-1"></a><span class="co"># variable, which is quite common in Makefiles</span></span>
<span id="cb5-20"><a href="#cb5-20" aria-hidden="true" tabindex="-1"></a><span class="dt">BIN</span> <span class="ch">?=</span><span class="st"> </span><span class="ch">$(</span><span class="dt">PREFIX</span><span class="ch">)</span><span class="st">/bin</span></span>
<span id="cb5-21"><a href="#cb5-21" aria-hidden="true" tabindex="-1"></a><span class="dt">MAN</span> <span class="ch">?=</span><span class="st"> </span><span class="ch">$(</span><span class="dt">PREFIX</span><span class="ch">)</span><span class="st">/share/man/man1</span></span>
<span id="cb5-22"><a href="#cb5-22" aria-hidden="true" tabindex="-1"></a><span class="co"># Install is a program in Linux and many other Unix-like systems</span></span>
<span id="cb5-23"><a href="#cb5-23" aria-hidden="true" tabindex="-1"></a><span class="co"># You can check whether install is a program on your machine</span></span>
<span id="cb5-24"><a href="#cb5-24" aria-hidden="true" tabindex="-1"></a><span class="co"># by running `which install`, my computer shows it is installed</span></span>
<span id="cb5-25"><a href="#cb5-25" aria-hidden="true" tabindex="-1"></a><span class="co"># at `/usr/local/bin/install`</span></span>
<span id="cb5-26"><a href="#cb5-26" aria-hidden="true" tabindex="-1"></a><span class="dt">INSTALL</span> <span class="ch">?=</span><span class="st"> install</span></span>
<span id="cb5-27"><a href="#cb5-27" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-28"><a href="#cb5-28" aria-hidden="true" tabindex="-1"></a><span class="dt">PROG</span> <span class="ch">=</span><span class="st"> xsct</span></span>
<span id="cb5-29"><a href="#cb5-29" aria-hidden="true" tabindex="-1"></a><span class="dt">SRCS</span> <span class="ch">=</span><span class="st"> src/xsct.c</span></span>
<span id="cb5-30"><a href="#cb5-30" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-31"><a href="#cb5-31" aria-hidden="true" tabindex="-1"></a><span class="co"># This variable is setting the libraries that must be invoked</span></span>
<span id="cb5-32"><a href="#cb5-32" aria-hidden="true" tabindex="-1"></a><span class="co"># during compilation, in this case:</span></span>
<span id="cb5-33"><a href="#cb5-33" aria-hidden="true" tabindex="-1"></a><span class="co"># X11 libraries, Xrandr libraries, and math libraries</span></span>
<span id="cb5-34"><a href="#cb5-34" aria-hidden="true" tabindex="-1"></a><span class="dt">LIBS</span> <span class="ch">=</span><span class="st"> -lX11 -lXrandr -lm</span></span>
<span id="cb5-35"><a href="#cb5-35" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-36"><a href="#cb5-36" aria-hidden="true" tabindex="-1"></a><span class="co"># This part of the Makefile is where the compilation happens</span></span>
<span id="cb5-37"><a href="#cb5-37" aria-hidden="true" tabindex="-1"></a><span class="co"># I will go through the specifics of this in a minute</span></span>
<span id="cb5-38"><a href="#cb5-38" aria-hidden="true" tabindex="-1"></a><span class="dv">$(PROG):</span><span class="dt"> </span><span class="ch">$(</span><span class="dt">SRCS</span><span class="ch">)</span></span>
<span id="cb5-39"><a href="#cb5-39" aria-hidden="true" tabindex="-1"></a><span class="er">    </span><span class="ch">$(</span><span class="dt">CC</span><span class="ch">)</span> <span class="ch">$(</span><span class="dt">CFLAGS</span><span class="ch">)</span> <span class="ch">$^</span> -o <span class="ch">$@</span> <span class="ch">$(</span><span class="dt">LDFLAGS</span><span class="ch">)</span> <span class="ch">$(</span><span class="dt">LIBS</span><span class="ch">)</span></span>
<span id="cb5-40"><a href="#cb5-40" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-41"><a href="#cb5-41" aria-hidden="true" tabindex="-1"></a><span class="co"># This section is describing what `make` should do if</span></span>
<span id="cb5-42"><a href="#cb5-42" aria-hidden="true" tabindex="-1"></a><span class="co"># `make install` is called</span></span>
<span id="cb5-43"><a href="#cb5-43" aria-hidden="true" tabindex="-1"></a><span class="dv">install:</span><span class="dt"> </span><span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span><span class="dt"> </span><span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span><span class="dt">.1</span></span>
<span id="cb5-44"><a href="#cb5-44" aria-hidden="true" tabindex="-1"></a><span class="er">    </span><span class="ch">$(</span><span class="dt">INSTALL</span><span class="ch">)</span> -d <span class="ch">$(</span><span class="dt">DESTDIR</span><span class="ch">)$(</span><span class="dt">BIN</span><span class="ch">)</span></span>
<span id="cb5-45"><a href="#cb5-45" aria-hidden="true" tabindex="-1"></a>    <span class="ch">$(</span><span class="dt">INSTALL</span><span class="ch">)</span> -m 0755 <span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span> <span class="ch">$(</span><span class="dt">DESTDIR</span><span class="ch">)$(</span><span class="dt">BIN</span><span class="ch">)</span></span>
<span id="cb5-46"><a href="#cb5-46" aria-hidden="true" tabindex="-1"></a>    <span class="ch">$(</span><span class="dt">INSTALL</span><span class="ch">)</span> -d <span class="ch">$(</span><span class="dt">DESTDIR</span><span class="ch">)$(</span><span class="dt">MAN</span><span class="ch">)</span></span>
<span id="cb5-47"><a href="#cb5-47" aria-hidden="true" tabindex="-1"></a>    <span class="ch">$(</span><span class="dt">INSTALL</span><span class="ch">)</span> -m 0644 <span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span>.1 <span class="ch">$(</span><span class="dt">DESTDIR</span><span class="ch">)$(</span><span class="dt">MAN</span><span class="ch">)</span></span>
<span id="cb5-48"><a href="#cb5-48" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-49"><a href="#cb5-49" aria-hidden="true" tabindex="-1"></a><span class="co"># This section is describing what `make` should do if</span></span>
<span id="cb5-50"><a href="#cb5-50" aria-hidden="true" tabindex="-1"></a><span class="co"># `make uninstall` is called</span></span>
<span id="cb5-51"><a href="#cb5-51" aria-hidden="true" tabindex="-1"></a><span class="dv">uninstall:</span></span>
<span id="cb5-52"><a href="#cb5-52" aria-hidden="true" tabindex="-1"></a><span class="er">    </span>rm -f <span class="ch">$(</span><span class="dt">BIN</span><span class="ch">)</span>/<span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span></span>
<span id="cb5-53"><a href="#cb5-53" aria-hidden="true" tabindex="-1"></a>    rm -f <span class="ch">$(</span><span class="dt">MAN</span><span class="ch">)</span>/<span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span>.1</span>
<span id="cb5-54"><a href="#cb5-54" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-55"><a href="#cb5-55" aria-hidden="true" tabindex="-1"></a><span class="dv">clean:</span></span>
<span id="cb5-56"><a href="#cb5-56" aria-hidden="true" tabindex="-1"></a><span class="er">    </span>rm -f <span class="ch">$(</span><span class="dt">PROG</span><span class="ch">)</span></span></code></pre></div>
<p>Often times, a large majority of a <code>Makefile</code> is just
getting variables set properly, this example is no different. Let’s
substitute the variables in the section that builds the software so it
is a bit more straightforward to read:</p>
<div class="sourceCode" id="cb6"><pre
class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="dv">xsct:</span><span class="dt"> src/xsct.c</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a><span class="er">    </span>gcc -Wall -Wextra -Werror -pedantic -std=c99 -O2 -I /usr/X11R6/include src/xsct.c -o xsct -L /usr/X11R6/lib -lX11 -lXrandr -lm</span></code></pre></div>
<p>After all of the variables are replaced with what they will evaluate
to (I know I glossed over the <code>$^</code> and <code>$@</code>, I
will come back to those), the build command in the <code>Makefile</code>
looks almost identical to the manual build command.</p>
<h1 id="applying-and-creating-patches-for-software">Applying and
Creating Patches for Software</h1>
<p>When working with source code, you may eventually come across
something that you want changed, modified, or removed. With proprietary
applications this is next to impossible, however, with open source
projects it is allowed and even encouraged. It is worth pointing out
that while patch management is not required if the changes are only for
you; utilizing version control and patching tools will make this process
much easier even if you do not plan on contributing your changes back to
the project (though you should if they are relevant).</p>
<h2 id="using-git">Using Git</h2>
<p>There are a lot of ways to go about version control and managing
patches, however, I am only going to go over two. The first one will be
some basic use of <a href="https://git-scm.com/">Git</a>, and the second
one will be more traditional Unix tools. I am going to start with Git as
it is the industry standard for version control and managing patches by
far. So, going back to the SCT project, let’s download it using
<code>git</code> if we have not already done so (may need to install
<code>git</code> on your machine). After that, we are going to make a
new branch that we can make changes to.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="co"># &#39;clone&#39; or download the repository (and history) using git</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> clone https://github.com/faf0/sct</span>
<span id="cb7-3"><a href="#cb7-3" aria-hidden="true" tabindex="-1"></a><span class="bu">cd</span> sct</span>
<span id="cb7-4"><a href="#cb7-4" aria-hidden="true" tabindex="-1"></a><span class="co"># Add new branch &#39;new-feature&#39;</span></span>
<span id="cb7-5"><a href="#cb7-5" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> branch new-feature</span>
<span id="cb7-6"><a href="#cb7-6" aria-hidden="true" tabindex="-1"></a><span class="co"># Switch to branch &#39;new-feature&#39;</span></span>
<span id="cb7-7"><a href="#cb7-7" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> checkout new-feature</span>
<span id="cb7-8"><a href="#cb7-8" aria-hidden="true" tabindex="-1"></a><span class="co"># Alternatively, you can create and switch to the new branch &#39;new-feature&#39;</span></span>
<span id="cb7-9"><a href="#cb7-9" aria-hidden="true" tabindex="-1"></a><span class="co"># in one command by running</span></span>
<span id="cb7-10"><a href="#cb7-10" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> checkout <span class="at">-b</span> new-feature</span></code></pre></div>
<p>Now that we have a new branch, we can begin making modifications to
the code. Just to make a simple example of this, I will be adding a line
to the README.md file. To get the diff of the changes I have made versus
what the master branch has, simply run:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> diff master</span></code></pre></div>
<p>The results of this, at least for the change I made, are as
follows:</p>
<div class="sourceCode" id="cb9"><pre
class="sourceCode diff"><code class="sourceCode diff"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="kw">diff --git a/README.md b/README.md</span></span>
<span id="cb9-2"><a href="#cb9-2" aria-hidden="true" tabindex="-1"></a>index afc027f..00b24df 100644</span>
<span id="cb9-3"><a href="#cb9-3" aria-hidden="true" tabindex="-1"></a><span class="dt">--- a/README.md</span></span>
<span id="cb9-4"><a href="#cb9-4" aria-hidden="true" tabindex="-1"></a><span class="dt">+++ b/README.md</span></span>
<span id="cb9-5"><a href="#cb9-5" aria-hidden="true" tabindex="-1"></a><span class="dt">@@ -21,6 +21,7 @@ Minor modifications were made in order to get sct to:</span></span>
<span id="cb9-6"><a href="#cb9-6" aria-hidden="true" tabindex="-1"></a> - return `EXIT_SUCCESS`</span>
<span id="cb9-7"><a href="#cb9-7" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb9-8"><a href="#cb9-8" aria-hidden="true" tabindex="-1"></a> # Installation</span>
<span id="cb9-9"><a href="#cb9-9" aria-hidden="true" tabindex="-1"></a><span class="va">+This is a new line to the README file.</span></span>
<span id="cb9-10"><a href="#cb9-10" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb9-11"><a href="#cb9-11" aria-hidden="true" tabindex="-1"></a> ## Make-based</span></code></pre></div>
<p>To make a proper patch of the changes that were made, we need to
commit the changes to the new branch, then make a patch file. A ‘commit’
is the way of telling git that you are happy with the changes and you
would like them to be submitted into the tree. Essentially, this is the
way to ‘save’ the file and be able to track those specific changes in
the version history. Making a commit and a patch file can be done by
running:</p>
<div class="sourceCode" id="cb10"><pre
class="sourceCode sh"><code class="sourceCode bash"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a><span class="co"># This command adds the new or modified files to be commited</span></span>
<span id="cb10-2"><a href="#cb10-2" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> add README.md</span>
<span id="cb10-3"><a href="#cb10-3" aria-hidden="true" tabindex="-1"></a><span class="co"># This command makes the commit, the argument &#39;-m&#39; will make</span></span>
<span id="cb10-4"><a href="#cb10-4" aria-hidden="true" tabindex="-1"></a><span class="co"># the commit message inline. Without this argument</span></span>
<span id="cb10-5"><a href="#cb10-5" aria-hidden="true" tabindex="-1"></a><span class="co"># git will open your $EDITOR for the commit message to be</span></span>
<span id="cb10-6"><a href="#cb10-6" aria-hidden="true" tabindex="-1"></a><span class="co"># typed out</span></span>
<span id="cb10-7"><a href="#cb10-7" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> commit <span class="at">-m</span> <span class="st">&quot;Some helpful message&quot;</span></span>
<span id="cb10-8"><a href="#cb10-8" aria-hidden="true" tabindex="-1"></a><span class="co"># This command will format the patch file and compare</span></span>
<span id="cb10-9"><a href="#cb10-9" aria-hidden="true" tabindex="-1"></a><span class="co"># the current commit branch to the master branch</span></span>
<span id="cb10-10"><a href="#cb10-10" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> format-patch origin/master</span></code></pre></div>
<p>The output of the last command will be a file that contains the diff
as well as the commit message for the commits ahead of the master
branch. Essentially, just describing the differences between the current
branch and the master branch. From there, that patch file can be stowed
away, emailed, or otherwise shared. Now let’s see how to apply that
patch to the code base:</p>
<div class="sourceCode" id="cb11"><pre
class="sourceCode sh"><code class="sourceCode bash"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Switches back to the master branch to &#39;undo&#39; our changes</span></span>
<span id="cb11-2"><a href="#cb11-2" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> checkout master</span>
<span id="cb11-3"><a href="#cb11-3" aria-hidden="true" tabindex="-1"></a><span class="co"># Creating a test branch, it is bad practice to modify the master branch directly</span></span>
<span id="cb11-4"><a href="#cb11-4" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> checkout <span class="at">-b</span> apply-test</span>
<span id="cb11-5"><a href="#cb11-5" aria-hidden="true" tabindex="-1"></a><span class="co"># Then apply the patch, it is worth noting that the .patch file</span></span>
<span id="cb11-6"><a href="#cb11-6" aria-hidden="true" tabindex="-1"></a><span class="co"># may have a different name that what I have here</span></span>
<span id="cb11-7"><a href="#cb11-7" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> apply 0001-Test-change.patch</span></code></pre></div>
<p>This is the absolute basics of dealing with patches in git; there is
a lot about <code>git</code> that I did not cover, because it would make
this blog post a medium length novel. So, rather than boring you, I will
just provide places to get more information on working with Git and open
source code:</p>
<ul>
<li><a href="https://git-scm.com/book/en/v2"><code>Git</code>
documentation</a></li>
<li><a
href="https://github.com/freeCodeCamp/how-to-contribute-to-open-source">How
to contribute to open source</a></li>
<li>Many projects have a ‘how to contribute’ section in their
documentation</li>
</ul>
<h2 id="manual-way-of-dealing-with-patches">Manual way of dealing with
patches</h2>
<p>In the interest of history, I am also going to give a quick overview
of how to deal with patches without using <code>git</code>. This is not
something I recommend doing without a reason as it is quite a bit more
painful, at least in my opinion. It also does not transfer to many open
source projects the same way that learning <code>git</code> does;
however, it is important to keep the old ways in mind.</p>
<p>So, let’s go back before any changes were made to any files for the
application. Before we actually work on any changes, we need to copy the
file(s) that we are going to work on to another file; generally, the
naming convention is to just append the filename with ‘.orig’, but
anything will work as long as you keep up with it. After making the
desired changes (will be same changes as the previous section) you can
run the following command:</p>
<div class="sourceCode" id="cb12"><pre
class="sourceCode sh"><code class="sourceCode bash"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true" tabindex="-1"></a><span class="co"># The &#39;-u&#39; flag is to make the output of the diff</span></span>
<span id="cb12-2"><a href="#cb12-2" aria-hidden="true" tabindex="-1"></a><span class="co"># look more similar to the git output, I find it easier</span></span>
<span id="cb12-3"><a href="#cb12-3" aria-hidden="true" tabindex="-1"></a><span class="co"># read than the default output that diff uses</span></span>
<span id="cb12-4"><a href="#cb12-4" aria-hidden="true" tabindex="-1"></a><span class="fu">diff</span> <span class="at">-u</span> README.md.orig README.md</span>
<span id="cb12-5"><a href="#cb12-5" aria-hidden="true" tabindex="-1"></a><span class="co"># To save the output into a file, simply use the Unix file redirection</span></span>
<span id="cb12-6"><a href="#cb12-6" aria-hidden="true" tabindex="-1"></a><span class="fu">diff</span> <span class="at">-u</span> README.md.orig README.md <span class="op">&gt;</span> README.md.patch</span></code></pre></div>
<p>The patch file can then be sent and shared around similar to the
patch file using <code>git</code>. Now how do we apply these patches to
a code base? With a utility called <code>patch</code>, that will likely
not be on the machine by default, thus will need to be installed. Once
installed, patches can be applied as follows:</p>
<div class="sourceCode" id="cb13"><pre
class="sourceCode sh"><code class="sourceCode bash"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true" tabindex="-1"></a><span class="co"># Notice the &#39;&lt;&#39; rather than the &#39;&gt;&#39;, if the &#39;&gt;&#39; is used, the program will just hang</span></span>
<span id="cb13-2"><a href="#cb13-2" aria-hidden="true" tabindex="-1"></a><span class="fu">patch</span> <span class="op">&lt;</span> README.md.patch README.md</span></code></pre></div>
<h1 id="closing-thoughts">Closing thoughts</h1>
<p>Working with source code can be intimidating at times, especially if
you are not used to doing it. Hopefully, this post helps alleviate some
of the fears and concerns with working with source code, and with making
patches to software.</p>
</article>
<footer>
    <p>
        Comments or suggestions?<br />
        Contact me: <a href="mailto:tyler.clark@foxide.xyz">tyler.clark@foxide.xyz</a>
    </p>
</footer>
